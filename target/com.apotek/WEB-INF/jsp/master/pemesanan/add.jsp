<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href='<c:url value="/resources/css/datepicker.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="<c:url value="/master/home"/>">Home</a></li>
		<li class="active">Pemesanan</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4">
			<div class="graphs">
				<h3 class="blank1">Form Pemesanan Obat</h3>
				<div class="tab-content">
					<div class="tab-pane active" id="horizontal-form">
						<form action="add" method="post" class="form-horizontal" id="formPesan">
							<div class="form-group">
								<label class="col-sm-2 control-label">No Pemesanan</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="noPemesanan" placeholder="No Pemesanan">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Tanggal</label>
								<div class="col-lg-3">
									<input type="text" class="form-control1" name="tglPemesanan" id="date" placeholder="Tanggal Pemesanan">
								</div>
							</div>
							<div class="form-group">
								<label for="selector1" class="col-sm-2 control-label">Nama Supplier</label>
								<div class="col-sm-5">
									<select name="supplier" class="form-control1">
										<option value="">-- Pilih Supplier --</option>
										<c:forEach items="${suppliers }" var="supplier">
											<option value="${supplier.id }">${supplier.namaSupplier }</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="selector1" class="col-sm-2 control-label">Kode Obat</label>
								<div class="col-sm-5">
									<select name="obat" id="obat" class="form-control1">
										<option value="">-- Pilih Kode Obat --</option>
										<c:forEach items="${obat }" var="obat">
											<option value="${obat.id }">${obat.kode }</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Obat</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="namaObat" id="namaObat" placeholder="Nama Obat" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jenis Obat</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="jenisObat" id ="jenisObat" placeholder="Jenis Obat" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Satuan</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="satuan" id="satuan" placeholder="Satuan" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jumlah Pesan</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="jmlPemesanan" placeholder="Jumlah Pemesanan">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-5">
									<button type="submit" class="btn btn-custom btn-sm">Save</button>
									<button type="reset" class="btn btn-custom btn-sm">Reset</button>
								</div>
							</div>
						</form>
						<button class="btn btn-custom btn-sm" id="k">KKKKKK</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="<c:url value='/resources/js/bootstrap-datepicker.js'/>"></script>
	<script src="<c:url value='/resources/js/bootstrapValidator.js'/>"></script>
    <script type="text/javascript">
    $(document).ready(function() {
    	$('#k').click(function() {
    		var date = new Date();
    		var mont = ["January", "February", "March", "April", "May", "June",
    		            "July", "August", "September", "October", "November", "December"];
    		var tgl = date.getDate() + "-" + mont[date.getMonth()] + "-" + date.getFullYear();
    		$('#date').val(tgl);
    	});
		$('#date').datepicker({
			format: "dd-MM-yyyy",
			autoclose: true,
			todayHighlight: true
		});
		$('#obat').change(function(){
			var id = $(this).val();
			$.ajax({
				type: 'post',
				url: 'get',
				cahce: false,
				data: ({id : id}),
				success: function(response) {
					$('#namaObat').val(response.namaObat);
					$('#jenisObat').val(response.jenisObat.jenis);
					$('#satuan').val(response.satuanObat.satuan);
				}
			});
		});
		$('#formPesan').bootstrapValidator({
			container: 'tooltip',
    		feedbackIcons: {
                valid: ' ',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
			fields: {
				noPemesanan: {
					validators: {
						notEmpty: {
	                        message: 'isi no pemesanan'
	                    }
					}
				},
				supplier: {
					validators: {
						notEmpty: {
							message: 'Pilih Supplier'
						}
					}
				},
				obat: {
					validators: {
						notEmpty: {
							message: 'pilih obat yang akan dipesan'
						}
					}
				},
				jmlPemesanan: {
					validators: {
						notEmpty: {
							message: 'masukkan jumlah obat yang akan dipesan'
						}
					}
				},
				tglPemesanan: {
					validators: {
						notEmpty: {
							message: 'Tanggal Pemsanan tidak boleh kosong'
						}
					}
				}
			}
		});
		$('button[type="submit"]').click(function() {
	        $('#defaultForm').bootstrapValidator('validate');
	    });

	    $('button[type="reset"]').click(function() {
	        $('#defaultForm').data('bootstrapValidator').resetForm(true);
	    });
	});
    
    function x(){
    	$('button[type="submit"]').attr('disabled','disabled');
		var isDisable = $('button[type="submit"]').is(':disabled');
		if (isDisable) {
			$('button[type="submit"]').removeAttr('disabled');
		}
    }
    </script>
</body>
</html>