<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href='<c:url value="/resources/css/dataTables.bootstrap.min.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="<c:url value="/master/home"/>">Home</a></li>
		<li><a href="<c:url value="/master/pemesanan/add"/>">pemesanan</a></li>
		<li class="active">List</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4" data-example-id="contextual-table">
			<h3 class="blank1">Pemesanan Obat</h3>
			<table id="example" class="table table-bordered">
				<thead>
					<tr>
						<th>Id</th>
						<th>Tanggal</th>
						<th>No Pemesanan</th>
						<th>Nama Supplier</th>
						<th>Nama Obat</th>
						<th>Satuan</th>
						<th>Jumlah Pesan</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pemesanan }" var="pesan">
						<tr>
							<td>${pesan.id }</td>
							<td><fmt:formatDate value="${pesan.tglPemesanan }" pattern="dd-MMMM-yyyy"/></td>
							<td>${pesan.noPemesanan }</td>
							<td>${pesan.supplier.namaSupplier }</td>
							<td>${pesan.obat.namaObat }</td>
							<td>${pesan.obat.satuanObat.satuan }</td>
							<td>${pesan.jmlPemesanan }</td>
							<td><a href="edit?id=${pesan.id }" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o"></i></a> | 
								<a href="delete?id=${pesan.id }" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
	<script src="<c:url value='/resources/js/jquery.dataTables.min.js'/>"></script>
	<script src="<c:url value='/resources/js/dataTables.bootstrap.min.js'/>"></script>
	<script type="text/javascript">
		$(document).ready(function() {
	    	$('#example').DataTable();
	    	$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
</body>
</html>
