<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href='<c:url value="/resources/css/datepicker.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="<c:url value="/master/home"/>">Home</a></li>
		<li><a href="<c:url value="/master/pemesananJTempo/add"/>">Pemesanan Jatuh Tempo</a></li>
		<li><a href="<c:url value="/master/pemesananJTempo/list"/>">List Pemesanan</a></li>
		<li class="active">Laporan</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4">
			<div class="graphs">
				<h3 class="blank1">Cetak Laporan Penjualan Obat</h3>
				<div class="tab-content">
					<div class="tab-pane active" id="horizontal-form">
						<form action="report" method="post" class="form-horizontal" target="_blank">
							<div class="form-group">
								<label class="col-sm-2 control-label">Tanggal</label>
								<div class="col-lg-3">
									<input type="text" class="form-control1 date" name="tgl_awal" placeholder="Tanggal Awal">
								</div>
								<div class="col-lg-3">
									<input type="text" class="form-control1 date" name="tgl_akhir" placeholder="Tanggal Akhir">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-5">
									<button type="submit" class="btn btn-custom btn-sm">Cetak Laporan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="<c:url value='/resources/js/bootstrap-datepicker.js'/>"></script>
    <script type="text/javascript">
    $(document).ready(function() {
		$('.date').datepicker({
			format: "dd-MM-yyyy",
			autoclose: true,
			todayHighlight: true
		});
	});
    </script>
</body>
</html>