<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href='<c:url value="/resources/css/dataTables.bootstrap.min.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="/master/home">Home</a></li>
		<li class="active">Penerimaan Obat</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4" data-example-id="contextual-table">
<!-- 			<a class="btn btn-custom" href="add">Add User</a> -->
			<br><br>
			<table id="example" class="table table-bordered">
				<thead>
					<tr>
						<th>Id</th>
						<th>No Faktur</th>
						<th>Tanggal Pemesanan</th>
						<th>Tanggal Jatuh Tempo</th>
						<th>Nama Supplier</th>
						<th>Nama Obat</th>
						<th>Satuan</th>
						<th>Jumlah</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${jTempo }" var="jTempo">
						<tr>
							<td>${jTempo.id }</td>
							<td>${jTempo.noFaktur }</td>
							<td><fmt:formatDate value="${jTempo.pemesanan.tglPemesanan }" pattern="dd-MM-yyyy"/></td>
							<td><fmt:formatDate value="${jTempo.tglJTempo }" pattern="dd-MM-yyyy"/></td>
							<td>${jTempo.pemesanan.supplier.namaSupplier }</td>
							<td>${jTempo.pemesanan.obat.namaObat }</td>
							<td>${jTempo.pemesanan.obat.satuanObat.satuan }</td>
							<td>${jTempo.pemesanan.jmlPemesanan }</td>
							<td><a href="edit?id=${jTempo.id }" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil-square-o"></i></a> | 
								<a href="delete?id=${jTempo.id }" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-times"></i></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
	<script src="<c:url value='/resources/js/jquery.dataTables.min.js'/>"></script>
	<script src="<c:url value='/resources/js/dataTables.bootstrap.min.js'/>"></script>
	<script type="text/javascript">
		$(document).ready(function() {
	    	$('#example').DataTable();
	    	$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
</body>
</html>
