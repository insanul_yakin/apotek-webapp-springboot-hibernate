<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href='<c:url value="/resources/css/datepicker.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="#">Home</a></li>
		<li><a href="#">Library</a></li>
		<li class="active">Data</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4">
			<div class="graphs">
				<h3 class="blank1">Form Penerimaan Obat</h3>
				<div class="tab-content">
					<div class="tab-pane active" id="horizontal-form">
						<form action="add" method="post" class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-2 control-label">ID</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="id" value="${jTempo.id }" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">No Faktur</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="noFaktur" value="${jTempo.noFaktur }">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">No Pemesanan</label>
								<div class="col-sm-5">
									<select name="pemesanan" class="form-control1">
										<option value="${jTempo.pemesanan.id }">${jTempo.pemesanan.noPemesanan }</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Supplier</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" value="${jTempo.pemesanan.supplier.namaSupplier }" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Obat</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" value="${jTempo.pemesanan.obat.namaObat }" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Tanggal Jatuh Tempo</label>
								<div class="col-lg-3">
									<input type="text" class="form-control1" name="tglJTempo" id="date" value="<fmt:formatDate value='${jTempo.tglJTempo }' pattern='dd-MMMM-yyyy'/>" readonly>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-5">
									<button type="submit" class="btn btn-custom btn-sm">Save</button>
									<button type="reset" class="btn btn-custom btn-sm">Reset</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="<c:url value='/resources/js/bootstrap-datepicker.js'/>"></script>
    <script type="text/javascript">
    $(document).ready(function() {
		$('#date').datepicker({
			format: 'dd-MM-yyyy',
			autoclose: true,
			todayHighlight: true
		});
	});
    </script>
</body>
</html>