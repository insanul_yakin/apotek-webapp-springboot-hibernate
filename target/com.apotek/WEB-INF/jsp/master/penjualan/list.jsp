<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href='<c:url value="/resources/css/dataTables.bootstrap.min.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="/master/home">Home</a></li>
		<li class="active">Penerimaan Obat</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4" data-example-id="contextual-table">
			<br><br>
			<table id="example" class="table table-bordered">
				<thead>
					<tr>
						<th>Id</th>
						<th>Tanggal</th>
						<th>Nama Customer</th>
						<th>Total Harga</th>
						<th>Resep</th>
						<th>Print Retur</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${listPenjualan }" var="penjualan">
						<tr>
							<td>${penjualan.id }</td>
							<td><fmt:formatDate value="${penjualan.tgl }" pattern="dd-MMMM-yyyy"/></td>
							<td><a href="detail?id=${penjualan.id }">${penjualan.nama }</a></td>
							<td>${penjualan.totalHarga }</td>
							<td> <c:out value="${penjualan.resep == '1' ? 'Resep' : 'Non Resep'}" /></td>
							<td><a href="retur?id=${penjualan.id }" target="_blank">Print</a>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
	<script src="<c:url value='/resources/js/jquery.dataTables.min.js'/>"></script>
	<script src="<c:url value='/resources/js/dataTables.bootstrap.min.js'/>"></script>
	<script type="text/javascript">
		$(document).ready(function() {
	    	$('#example').DataTable();
		});
	</script>
</body>
</html>
