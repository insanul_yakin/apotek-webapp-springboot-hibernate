<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href='<c:url value="/resources/css/datepicker.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="<c:url value="/master/home"/>">Home</a></li>
		<li class="active">Penerimaan</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4">
			<div class="graphs">
				<h3 class="blank1">Form Penerimaan Obat</h3>
				<div class="tab-content">
					<div class="tab-pane active" id="horizontal-form">
						<form action="add" method="post" class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-2 control-label">No Pemesanan</label>
								<div class="col-sm-5">
									<select name="pemesanan" id="noPemesanan" class="form-control1">
										<option>-- No Pemesanan --</option>
										<c:forEach items="${pemesanan }" var="pemesanan">
											<c:if test="${pemesanan.status != 1 }">
												<option value="${pemesanan.id }">${pemesanan.noPemesanan }</option>
											</c:if>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Supplier</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" id="namaSupplier" placeholder="Nama Supplier" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Obat</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" id="namaObat" placeholder="Nama Obat" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jenis Obat</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" id="jenisObat" placeholder="Jenis Obat" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">satuan Obat</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" id="satuanObat" placeholder="Satuan Obat" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jumlah Pemesanan</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" id="jmlPesan" placeholder="Jumlah Pemesanan" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jumlah Terima</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="jmlTerima" id="jml" placeholder="Jumlah Penerimaan">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Tanggal Terima</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="tglTerima" id="date" placeholder="Tanggal Penerimaan" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Harga Satuan</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="hargaSatuan" id="harga" placeholder="harga satuan">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Total Harga</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="totalHarga" id="total" placeholder="Total Harga" readonly>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-5">
									<button type="submit" class="btn btn-custom btn-sm">Save</button>
									<button type="reset" class="btn btn-custom btn-sm">Reset</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="<c:url value='/resources/js/bootstrap-datepicker.js'/>"></script>
    <script type="text/javascript">
    $(document).ready(function() {
		$('#date').datepicker({
			format: "dd-MM-yyyy",
			autoclose: true,
			todayHighlight: true
		});
		$('#noPemesanan').change(function(){
			var id = $(this).val();
			$.ajax({
				type: 'post',
				url: 'get',
				cahce: false,
				data: ({id : id}),
				success: function(response) {
					$('#namaSupplier').val(response.supplier.namaSupplier);
					$('#namaObat').val(response.obat.namaObat);
					$('#jenisObat').val(response.obat.jenisObat.jenis);
					$('#satuanObat').val(response.obat.satuanObat.satuan);
					$('#jmlPesan').val(response.jmlPemesanan);
				}
			});
		});
		$('#harga').change(function() {
			x = $(this).val();
			y = $('#jml').val();
			$('#total').val(x * y);
		});
	});
    </script>
</body>
</html>