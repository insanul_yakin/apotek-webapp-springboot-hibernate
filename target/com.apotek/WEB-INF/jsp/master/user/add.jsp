<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="<c:url value='/master/home'/>">Home</a></li>
		<li><a href="<c:url value='/master/user/list'/>">Data User</a></li>
		<li class="active">Add User</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4">
			<div class="graphs">
				<h3 class="blank1">Add User</h3>
				<div class="tab-content">
					<div class="tab-pane active" id="horizontal-form">
						<form class="form-horizontal" action="add" method="post" id="formUser">
							<div class="form-group">
								<label class="col-sm-2 control-label">Username</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="username" id="username" placeholder="username" required>
									<div id="result"></div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Password</label>
								<div class="col-lg-5">
									<input type="password" class="form-control1" name="password" placeholder="password" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Confirm Password</label>
								<div class="col-lg-5">
									<input type="password" class="form-control1" name="confirmPassword" placeholder="Ulangi Password" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Status</label>
								<div class="col-lg-5">
									<div id="status" class="btn-group" data-toggle="buttons">
										<label class="btn btn-custom" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
											<input type="radio" name="status" value="1" required> &nbsp; Aktif &nbsp;
										</label>
										<label class="btn btn-custom" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
											<input type="radio" name="status" value="0" class=""> Tidak Aktif
										</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="selector1" class="col-sm-2 control-label">Role</label>
								<div class="col-sm-5">
									<select name="role" id="role" class="form-control1">
										<option value=" ">-- Role --</option>
										<c:forEach items="${roles }" var="role">
											<option value="${role.id }">${role.authority }</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-5">
									<button type="submit" class="btn btn-custom btn-sm">Save</button>
									<button type="reset" class="btn btn-custom btn-sm" id="resetBtn">Reset</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<script src="<c:url value='/resources/js/bootstrapValidator.js'/>"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#username').change(function(){
				var name = $(this).val();
				$.ajax({
					type: 'get',
					url: 'username',
					cahce: false,
					data: ({name : name}),
					success: function(response) {
						$('#result').html(response);
					}
				});
			});
			$('#formUser').bootstrapValidator({
		        fields: {
		        	username: {
		                validators: {
		                    notEmpty: {
		                        message: 'Username tidak boleh kosong'
		                    },
		                    stringLength: {
		                        min: 6,
		                        max: 30,
		                        message: 'username minimal 6 dan maksimal 30 karakter'
		                    },
		                    regexp: {
		                        regexp: /^[a-zA-Z0-9_\.]+$/,
		                        message: 'Username hanya bisa terdiri dari abjad, angka, titik dan garis bawah'
		                    }
		                }
		            },
		            password: {
		                validators: {
		                    notEmpty: {
		                        message: 'Password tidak boleh kosong'
		                    },
		                    different: {
		                        field: 'username',
		                        message: 'Password tidak boleh sama dengan username'
		                    }
		                }
		            },
		            confirmPassword: {
		                validators: {
		                    notEmpty: {
		                        message: 'Confirm password tidak boleh kosong'
		                    },
		                    identical: {
		                        field: 'password',
		                        message: 'password tidak sama'
		                    }
		                }
		            },
		            status: {
		                validators: {
		                    notEmpty: {
		                        message: 'Pilih Status'
		                    }
		                }
		            },
		           	role: {
		                validators: {
		                	notEmpty: {
		                        message: 'Pilih Role'
		                    }
		                }
		            }
		        }
			});
			$('#resetBtn').click(function() {
		        $('#formUser').data('bootstrapValidator').resetForm(true);
		    });
		});
    </script>

</body>
</html>