<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href='<c:url value="/resources/css/dataTables.bootstrap.min.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="/master/home">Home</a></li>
		<li class="active">Data Obat</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4" data-example-id="contextual-table">
			<a class="btn btn-custom" href="add"><i class="fa fa-plus"></i> Obat</a>
			<a class="btn btn-custom" href="tipe"><i class="fa fa-plus"></i> Tipe Obat</a>
			<a class="btn btn-custom" href="jenis"><i class="fa fa-plus"></i> Jenis Obat</a>
			<a class="btn btn-custom" href="satuan"><i class="fa fa-plus"></i> Satuan Obat</a>
			<br><br>
			<table id="example" class="table table-bordered">
				<thead>
					<tr>
						<th>Id</th>
						<th>Kode Obat</th>
						<th>Tipe Obat</th>
						<th>Nama Obat</th>
						<th>Jenis Obat</th>
						<th>Satuan Obat</th>
						<th>Harga Beli</th>
						<th>Harga Jual</th>
						<th>Stok Obat</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${obat }" var="obat">
						<tr>
							<td>${obat.id }</td>
							<td>${obat.kode }</td>
							<td>${obat.tipeObat.tipe }</td>
							<td>${obat.namaObat }</td>
							<td>${obat.jenisObat.jenis }</td>
							<td>${obat.satuanObat.satuan }</td>
							<td>${obat.hargaBeli }</td>
							<td>${obat.hargaJual }</td>
							<td>${obat.stok }</td>
							<td><a href="edit?id=${obat.id }" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o"></i></a> | 
								<a href="delete?id=${obat.id }" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
	<script src="<c:url value='/resources/js/jquery.dataTables.min.js'/>"></script>
	<script src="<c:url value='/resources/js/dataTables.bootstrap.min.js'/>"></script>
	<script type="text/javascript">
		$(document).ready(function() {
	    	$('#example').DataTable();
	    	$('[data-toggle="tooltip"]').tooltip(); 
		});
	</script>
</body>
</html>