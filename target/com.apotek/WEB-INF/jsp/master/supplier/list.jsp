<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Data Supplier</title>
<!-- <link href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css" rel="stylesheet"> -->
<link href='<c:url value="/resources/css/dataTables.bootstrap.min.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="<c:url value='/master/home'/>">Home</a></li>
		<li class="active">Data Supplier</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4" data-example-id="contextual-table">
			<a class="btn btn-custom" href="add">Tambah Suppliers</a>
			<br><br>
			<table id="example" class="table table-bordered">
				<thead>
					<tr>
						<th>Id</th>
						<th>Nama Supplier</th>
						<th>Alamat Supplier</th>
						<th>Kota Supplier</th>
						<th>Telepon Supplier</th>
						<th>E-Mail</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${suppliers }" var="supplier">
						<tr>
							<td>${supplier.id }</td>
							<td>${supplier.namaSupplier }</td>
							<td>${supplier.alamatSupplier }</td>
							<td>${supplier.kotaSupplier }</td>
							<td>${supplier.tlpSupplier }</td>
							<td>${supplier.email }</td>
							<td><a href="edit?id=${supplier.id }"><i class="fa fa-pencil-square-o"></i></a> | 
								<a href="delete?id=${supplier.id }"><i class="fa fa-times"></i></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
	<script src="<c:url value='/resources/js/jquery.dataTables.min.js'/>"></script>
<!-- 	<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script> -->
	<script src="<c:url value='/resources/js/dataTables.bootstrap.min.js'/>"></script>
<!-- 	<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script> -->
	<script type="text/javascript">
		$(document).ready(function() {
	    	$('#example').DataTable();
		});
	</script>
</body>
</html>