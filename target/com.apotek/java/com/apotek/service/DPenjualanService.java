package com.apotek.service;

import com.apotek.entity.DetilPenjualan;

import java.util.List;

public interface DPenjualanService {
	
	public void add(DetilPenjualan detilPenjualan);
	
	public List<DetilPenjualan> detilPenjualan(int idPenjualan);

}
