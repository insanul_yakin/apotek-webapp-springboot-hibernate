package com.apotek.service;

import com.apotek.entity.TipeObat;

import java.util.List;

public interface TipeObatService {
	
	public void add(TipeObat tipeObat);
	
	public List<TipeObat> listTipeObat();
	
	public TipeObat getById(int id);
	
	public void del(TipeObat tipeObat);

}
