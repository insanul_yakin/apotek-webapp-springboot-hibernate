package com.apotek.service;

import com.apotek.entity.Penerimaan;

import java.util.List;

public interface PenerimaanService {
	
	public void add(Penerimaan penerimaan);
	
	public List<Penerimaan> listPenerimaan();
	
	public Penerimaan getById(int id);
	
	public void del(Penerimaan penerimaan);

}
