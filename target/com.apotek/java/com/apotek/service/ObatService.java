package com.apotek.service;

import com.apotek.entity.Obat;

import java.util.List;

public interface ObatService {
	
	public void add(Obat obat);
	
	public List<Obat> listObat();
	
	public void delete(Obat obat);
	
	public Obat getById(int id);

}
