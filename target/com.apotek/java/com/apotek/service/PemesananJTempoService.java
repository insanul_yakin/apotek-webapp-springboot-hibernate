package com.apotek.service;

import com.apotek.entity.PemesananJTempo;

import java.util.List;

public interface PemesananJTempoService {
	
	public void add(PemesananJTempo jTempo);
	
	public List<PemesananJTempo> listPembelianJTempo();
	
	public PemesananJTempo getById(int id);
	
	public void del(PemesananJTempo jTempo);

}
