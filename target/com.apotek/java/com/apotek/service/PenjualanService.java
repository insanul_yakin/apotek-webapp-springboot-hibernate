package com.apotek.service;

import com.apotek.entity.Penjualan;

import java.util.List;

public interface PenjualanService {
	
	public void add(Penjualan penjualan);
	
	public List<Penjualan> listPenjualan();
	
//	public Penjualan getById(int id);

}
