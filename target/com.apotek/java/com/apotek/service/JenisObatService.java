package com.apotek.service;

import com.apotek.entity.JenisObat;

import java.util.List;

public interface JenisObatService {
	
	public void add(JenisObat jenisObat);
	
	public List<JenisObat> listJenisObat();
	
	public JenisObat getById(int id);
	
	public void delete(JenisObat jenisObat);

}
