package com.apotek.impl;

import com.apotek.entity.Penjualan;
import com.apotek.service.PenjualanService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("penjualanService")
@Transactional
public class PenjualanServiceImpl implements PenjualanService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void add(Penjualan penjualan) {
		sessionFactory.getCurrentSession().saveOrUpdate(penjualan);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Penjualan> listPenjualan() {
		return sessionFactory.getCurrentSession().createQuery("From Penjualan").list();
	}
	
//	@Override
//	public Penjualan getById(int id) {
//		return (Penjualan) sessionFactory.getCurrentSession().get(Penjualan.class, id);
//	}

}
