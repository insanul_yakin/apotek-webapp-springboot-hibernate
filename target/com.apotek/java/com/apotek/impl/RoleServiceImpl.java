package com.apotek.impl;

import com.apotek.entity.Role;
import com.apotek.service.RoleService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("roleService")
@Transactional
public class RoleServiceImpl implements RoleService {
	

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(Role role) {
		sessionFactory.getCurrentSession().saveOrUpdate(role);
	}
	
	@Override
	public void delete(Role role) {
		sessionFactory.getCurrentSession().delete(role);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Role> listRole() {
		return sessionFactory.getCurrentSession().createQuery("From Role").list();
	}
	
	@Override
	public Role getById(int id) {
		return (Role) sessionFactory.getCurrentSession().get(Role.class, id);
	}

}
