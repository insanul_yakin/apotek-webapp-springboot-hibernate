package com.apotek.impl;

import com.apotek.entity.Obat;
import com.apotek.service.ObatService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("obatService")
@Transactional
public class ObatServiceImpl implements ObatService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void add(Obat obat) {
		sessionFactory.getCurrentSession().saveOrUpdate(obat);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Obat> listObat() {
		return sessionFactory.getCurrentSession().createQuery("From Obat").list();
	}
	
	@Override
	public void delete(Obat obat) {
		sessionFactory.getCurrentSession().delete(obat);
	}
	
	@Override
	public Obat getById(int id) {
		return (Obat) sessionFactory.getCurrentSession().get(Obat.class, id);
	}

}
