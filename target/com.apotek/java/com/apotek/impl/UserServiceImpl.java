package com.apotek.impl;

import com.apotek.entity.User;
import com.apotek.service.UserService;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void add(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);
	}
	
	@Override	
	public void delete(User user) {
		sessionFactory.getCurrentSession().delete(user);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> listUsers() {
		return sessionFactory.getCurrentSession().createQuery("select distinct(u) from User u left join fetch u.role r").list();
	}
	
	@Override
	public User getUserById(int id) {
		String hql = "from User u left join fetch u.role r where u.id = :id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("id", id);
		return (User) query.uniqueResult();
	}

}
