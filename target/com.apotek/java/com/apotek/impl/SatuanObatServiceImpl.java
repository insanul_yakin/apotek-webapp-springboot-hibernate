package com.apotek.impl;

import com.apotek.entity.SatuanObat;
import com.apotek.service.SatuanObatService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("satuanObatService")
@Transactional
public class SatuanObatServiceImpl implements SatuanObatService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void add(SatuanObat satuanObat) {
		sessionFactory.getCurrentSession().saveOrUpdate(satuanObat);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SatuanObat> listSatuanObat() {
		return sessionFactory.getCurrentSession().createQuery("From SatuanObat").list();
	}
	
	@Override
	public SatuanObat getById(int id) {
		return (SatuanObat) sessionFactory.getCurrentSession().get(SatuanObat.class, id);
	}
	
	@Override
	public void del(SatuanObat satuanObat) {
		sessionFactory.getCurrentSession().delete(satuanObat);
	}

}
