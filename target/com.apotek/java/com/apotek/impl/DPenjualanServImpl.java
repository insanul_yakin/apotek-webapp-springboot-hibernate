package com.apotek.impl;

import com.apotek.entity.DetilPenjualan;
import com.apotek.service.DPenjualanService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("dPenjualanService")
@Transactional
public class DPenjualanServImpl implements DPenjualanService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void add(DetilPenjualan detilPenjualan) {
		sessionFactory.getCurrentSession().saveOrUpdate(detilPenjualan);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DetilPenjualan> detilPenjualan(int idPenjualan) {
		return sessionFactory.getCurrentSession().createQuery("From DetilPenjualan dp where dp.penjualan = :idPenjualan").setInteger("idPenjualan", idPenjualan).list();
	}

}
