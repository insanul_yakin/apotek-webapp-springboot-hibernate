package com.apotek.controller;

import com.apotek.entity.JenisObat;
import com.apotek.service.JenisObatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "master/*")
public class JenisObatController {
	
	@Autowired
	private JenisObatService jenisObatService;
	
	@RequestMapping(value = "obat/jenis", method = RequestMethod.GET)
	public String jenis(ModelMap modelMap) {
		List<JenisObat> jenisObat = jenisObatService.listJenisObat();
		modelMap.put("jenisObat", jenisObat);
		return "/master/obat/jenis-obat";
	}
	
	@RequestMapping(value = "obat/jenis", method = RequestMethod.POST)
	public String simpan(@ModelAttribute JenisObat jenisObat) {
		jenisObatService.add(jenisObat);
		return "redirect:jenis";
	}
	
	@RequestMapping(value = "obat/deleteJenis", method = RequestMethod.GET)
	public String delete(@ModelAttribute JenisObat jenisObat) {
		jenisObatService.delete(jenisObat);
		return "redirect:jenis";
	}
	
	@RequestMapping(value = "obat/editJenis", method = RequestMethod.POST)
	public @ResponseBody
    JenisObat edit(@RequestParam String name) {
		int id = Integer.parseInt(name);
		JenisObat jenisObat = jenisObatService.getById(id);
		return jenisObat;
	}

}
