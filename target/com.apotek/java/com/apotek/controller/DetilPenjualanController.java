package com.apotek.controller;

import com.apotek.entity.DetilPenjualan;
import com.apotek.entity.Obat;
import com.apotek.entity.Penjualan;
import com.apotek.service.DPenjualanService;
import com.apotek.service.ObatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "master/*")
public class DetilPenjualanController {
	
	@Autowired
	private ObatService obatService;
	
	@Autowired
	private DPenjualanService dPenjualanService;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "penjualan/addCart", method = RequestMethod.POST)
	public String add(@ModelAttribute DetilPenjualan dPenjualan, BindingResult result, HttpSession session) {
		int idObat = Integer.parseInt(result.getFieldValue("obat").toString());
		Obat obat = obatService.getById(idObat);
		dPenjualan.setObat(obat);
		
		List<DetilPenjualan> list = (List<DetilPenjualan>) session.getAttribute("cart");
		if (list == null) {
			list = new ArrayList<DetilPenjualan>();
			dPenjualan.setTotalHarga(getHarga(dPenjualan));
			list.add(dPenjualan);
		} else {
			boolean flag = false;
			for (DetilPenjualan dp : list) {
				int x = dp.getObat().getId();
				int y = dPenjualan.getObat().getId();
				if (x == y) {
					dp.setJmlBeli(dp.getJmlBeli()+dPenjualan.getJmlBeli());
					dp.setTotalHarga(getHarga(dp));
					flag = true;
					break;
				}
			}
			if (flag==false) {
				dPenjualan.setTotalHarga(getHarga(dPenjualan));
				list.add(dPenjualan);
			}
		}
		
		session.setAttribute("cart", list);
		session.setAttribute("total", getTotal(list));
		return "redirect:add";
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "penjualan/remove", method = RequestMethod.GET)
	public String remove(@ModelAttribute Obat obat, HttpSession session) {
		List<DetilPenjualan> list = (List<DetilPenjualan>) session.getAttribute("cart");
		for (DetilPenjualan dp : list) {
			if (dp.getObat().getKode().equals(obat.getKode())) {
				list.remove(dp);
				break;
			}
		}
		session.setAttribute("total", getTotal(list));
		return "redirect:add";
	}
	
	@RequestMapping(value = "penjualan/detail", method = RequestMethod.GET)
	public String detail(@ModelAttribute Penjualan penjualan, ModelMap map) {
		List<DetilPenjualan> listDetilPenjualan = dPenjualanService.detilPenjualan(penjualan.getId());
		map.put("detilPenjualan", listDetilPenjualan);
		return "/master/penjualan/detail-obat";
	}
	
	public double getHarga(DetilPenjualan detilPenjualan) {
		int jml = detilPenjualan.getJmlBeli();
		int hargaJual = Integer.parseInt(detilPenjualan.getObat().getHargaJual());
		double harga = jml * hargaJual;
		return harga;
	}
	
	public double getTotal(List<DetilPenjualan> list) {
		double total = 0;
		for (DetilPenjualan dp : list) {
			int jml = dp.getJmlBeli();
			int hargaJual = Integer.parseInt(dp.getObat().getHargaJual());
			total += (jml * hargaJual);
		}
		return total;
	}

}
