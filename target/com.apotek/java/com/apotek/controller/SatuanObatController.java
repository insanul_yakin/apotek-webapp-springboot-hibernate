package com.apotek.controller;

import com.apotek.entity.SatuanObat;
import com.apotek.service.SatuanObatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "master/*")
public class SatuanObatController {
	
	@Autowired
	private SatuanObatService satuanObatService;
	
	@RequestMapping(value = "obat/satuan", method = RequestMethod.GET)
	public String satuan(ModelMap modelMap) {
		List<SatuanObat> satuanObat = satuanObatService.listSatuanObat();
		modelMap.put("satuanObat", satuanObat);
		return "/master/obat/satuan-obat";
	}
	
	@RequestMapping(value = "obat/satuan", method = RequestMethod.POST)
	public String simpanSatuan(@ModelAttribute SatuanObat satuanObat) {
		satuanObatService.add(satuanObat);
		return "redirect:satuan";
	}
	
	@RequestMapping(value = "obat/deleteSatuan", method = RequestMethod.GET)
	public String delete(@ModelAttribute SatuanObat satuanObat) {
		satuanObatService.del(satuanObat);
		return "redirect:satuan";
	}
	
	@RequestMapping(value = "obat/editSatuan", method = RequestMethod.POST)
	public @ResponseBody
    SatuanObat editSatuan(@RequestParam String id) {
		int idSatuan = Integer.parseInt(id);
		SatuanObat satuanObat = satuanObatService.getById(idSatuan);
		return satuanObat;
	}

}
