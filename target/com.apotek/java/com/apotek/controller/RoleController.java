package com.apotek.controller;

import com.apotek.entity.Role;
import com.apotek.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "master/*")
public class RoleController {
	
	@Autowired
	private RoleService roleService;
	
	@RequestMapping(value="user/role", method = RequestMethod.GET)
	public String add(ModelMap modelMap) {
		List<Role> roles = roleService.listRole();
		modelMap.put("roles", roles);
		return "/master/user/role-user";
	}
	
	@RequestMapping(value="user/addRole", method = RequestMethod.POST)
	public String save(@ModelAttribute Role role) {
		roleService.create(role);
		return "redirect:role";
	}
	
	@RequestMapping(value="user/delRole", method = RequestMethod.GET)
	public String del(@ModelAttribute Role role) {
		roleService.delete(role);
		return "redirect:role";
	}
	
	@RequestMapping(value="user/getRole", method = RequestMethod.POST)
	public @ResponseBody
    Role getRole(@RequestParam String id) {
		int idRole = Integer.parseInt(id);
		Role role = roleService.getById(idRole);
		return role;
	}

}
