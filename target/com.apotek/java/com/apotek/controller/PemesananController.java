package com.apotek.controller;

import com.apotek.entity.Obat;
import com.apotek.entity.Pemesanan;
import com.apotek.entity.Supplier;
import com.apotek.entity.Tanggal;
import com.apotek.service.ObatService;
import com.apotek.service.PemesananService;
import com.apotek.service.SupplierService;
import com.apotek.util.Conn;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "master/*")
public class PemesananController {
	
	@Autowired
	private PemesananService pemesananService;
	
	@Autowired
	private SupplierService supplierService;
	
	@Autowired
	private ObatService obatService;
	
	@RequestMapping(value = "pemesanan/list")
	public String list(ModelMap modelMap) {
		List<Pemesanan> pemesanan = pemesananService.listPemesanan();
		modelMap.put("pemesanan", pemesanan);
		return "/master/pemesanan/list";
	}
	
	@RequestMapping(value = "pemesanan/add", method = RequestMethod.GET)
	public String add(ModelMap modelMap) {
		List<Supplier> suppliers = supplierService.listSuppliers();
		modelMap.put("suppliers", suppliers);
		
		List<Obat> obat = obatService.listObat();
		modelMap.put("obat", obat);
		
		return "/master/pemesanan/add";
	}
	
	@RequestMapping(value = "pemesanan/add", method = RequestMethod.POST)
	public String save(@ModelAttribute Pemesanan pemesanan, BindingResult result) {
		int idSupplier = Integer.parseInt(result.getFieldValue("supplier").toString());
		Supplier supplier = supplierService.getById(idSupplier);
		
		int idObat = Integer.parseInt(result.getFieldValue("obat").toString());
		Obat obat = obatService.getById(idObat);
		
		pemesanan.setSupplier(supplier);
		pemesanan.setObat(obat);
		pemesananService.add(pemesanan);
		return "redirect:list";
	}
	
	@RequestMapping(value = "pemesanan/edit", method = RequestMethod.GET)
	public String detail(@ModelAttribute Pemesanan pemesanan, ModelMap map) {
		List<Supplier> suppliers = supplierService.listSuppliers();
		map.put("suppliers", suppliers);
		
		List<Obat> obat = obatService.listObat();
		map.put("obat", obat);
		
		pemesanan = pemesananService.getById(pemesanan.getId());
		map.put("pesan", pemesanan);
		
		return "/master/pemesanan/detail";
	}
	
	@RequestMapping(value = "pemesanan/delete", method = RequestMethod.GET)
	public String del(@ModelAttribute Pemesanan pemesanan) {
		pemesananService.delete(pemesanan);
		return "redirect:list";
	}
	
	@RequestMapping(value = "pemesanan/report", method = RequestMethod.GET)
	public String report() {
		return "/master/pemesanan/report";
	}
	
	@RequestMapping(value = "pemesanan/report", method = RequestMethod.POST)
	public void cetak(@ModelAttribute Tanggal tgl, HttpServletResponse response, HttpSession session) throws JRException, IOException {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tgl_awal", tgl.getTgl_awal());
		param.put("tgl_akhir", tgl.getTgl_akhir());
		
		File file = new File(session.getServletContext().getRealPath("/reports/LaporanPemesanan.jasper"));
		byte[] bs = JasperRunManager.runReportToPdf(file.getPath(), param, Conn.getConnection());
		
		response.setContentType("application/pdf");
		response.setContentLength(bs.length);
		response.setHeader("content-disposition", "inline; filename=LaporanPemesanan.pdf");
		ServletOutputStream outputStream = response.getOutputStream();
		outputStream.write(bs, 0, bs.length);
//		outputStream.flush();
//		outputStream.close();
	}
	
	@RequestMapping(value = "pemesanan/get", method = RequestMethod.POST)
	public @ResponseBody
    Obat get(@RequestParam String id) {
		int idObat = Integer.parseInt(id);
		Obat obat = obatService.getById(idObat);
		return obat;
	}

}
