package com.apotek.controller;

import com.apotek.entity.Role;
import com.apotek.entity.User;
import com.apotek.service.RoleService;
import com.apotek.service.UserService;
import com.apotek.util.Encryption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "master/*")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;
	
	@RequestMapping(value = "user/list")
	public String list(ModelMap modelMap) {
		List<User> listUsers = userService.listUsers();
		modelMap.put("users", listUsers);
		return "/master/user/list";
	}
	
	@RequestMapping(value = "user/add", method = RequestMethod.GET)
	public String add(ModelMap modelMap) {
		List<Role> roles = roleService.listRole();
		modelMap.put("roles", roles);
		return "/master/user/add";
	}
	
	@RequestMapping(value = "user/add", method = RequestMethod.POST)
	public String save(@ModelAttribute User user, BindingResult result) {
		if (user.getPassword() == null) {
			User user2 = userService.getUserById(user.getId());
			user.setPassword(user2.getPassword());
		} else {
			String password = result.getFieldValue("password").toString();
			user.setPassword(Encryption.md5(password));
		}
		
		int idRole = Integer.parseInt(result.getFieldValue("role").toString());
		Role role = roleService.getById(idRole);
		user.getRole().add(role);
		userService.add(user);
		return "redirect:list";
	}
	
	@RequestMapping(value = "user/edit", method = RequestMethod.GET)
	public String edit(@ModelAttribute User user, ModelMap modelMap) {
		List<User> users = userService.listUsers();
		modelMap.put("users", users);
		
		List<Role> roles = roleService.listRole();
		modelMap.put("role", roles);
		
		user = userService.getUserById(user.getId());
		modelMap.put("user", user);
		return "/master/user/detail";
	}
	
	@RequestMapping(value = "user/changePasswd", method = RequestMethod.POST)
	public String change(@ModelAttribute User user, BindingResult result) {
		String password = result.getFieldValue("password").toString();
		user = userService.getUserById(user.getId());
		user.setPassword(Encryption.md5(password));
		userService.add(user);
		return "redirect:edit?id="+user.getId();
	}
	
	@RequestMapping(value = "user/delete", method = RequestMethod.GET)
	public String delete(@ModelAttribute User user) {
		userService.delete(user);
		return "redirect:list";
	}
	
	@RequestMapping(value = "user/username", method = RequestMethod.GET)
	public @ResponseBody
    String name(@RequestParam String name) {
		List<User> list = userService.listUsers();
		int i = 0;
		String result;
		try {
			while (true) {
				User user = list.get(i);
				if (user.getUsername().equalsIgnoreCase(name)) {
					result = "username suda digunakan";
					return result;
				}
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		result = "username belum digunakan";
		return result;
	}
	
	@RequestMapping(value = "user/getPasswd", method = RequestMethod.POST)
	public @ResponseBody
    String passwd(@ModelAttribute User user, BindingResult result) {
		String resultt = null;
		String password = Encryption.md5(result.getFieldValue("password").toString());
		user = userService.getUserById(user.getId());
		if (password.equals(user.getPassword())) {
			resultt = "1";
		} else {
			resultt = "0";
		}
		return resultt;
	}

}
