package com.apotek.entity;

import javax.persistence.*;

@Entity
@Table(name = "satuanObat")
public class SatuanObat {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@Column(name = "kode")
	private String kode;
	
	@Column(name = "satuan")
	private String satuan;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getKode() {
		return kode;
	}
	public void setKode(String kode) {
		this.kode = kode;
	}

	public String getSatuan() {
		return satuan;
	}
	public void setSatuan(String satuan) {
		this.satuan = satuan;
	}

}
