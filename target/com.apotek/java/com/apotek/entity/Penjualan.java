package com.apotek.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="penjualan")
public class Penjualan {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@Column(name="tgl")
	private Date tgl;
	
	@Column(name="resep")
	private Integer resep;
	
	@Column(name="nama")
	private String nama;
	
	@Column(name="total")
	private Double totalHarga;
		
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getResep() {
		return resep;
	}
	public void setResep(Integer resep) {
		this.resep = resep;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public Double getTotalHarga() {
		return totalHarga;
	}
	public void setTotalHarga(Double totalHarga) {
		this.totalHarga = totalHarga;
	}
	public Date getTgl() {
		return tgl;
	}
	public void setTgl(Date tgl) {
		this.tgl = tgl;
	}

}
