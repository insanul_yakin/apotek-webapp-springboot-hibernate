package com.apotek.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "pemesanan")
public class Pemesanan {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@Column(name="no_pemesanan")
	private String noPemesanan;
	
	@Column(name="tgl_pemesanan")
	private Date tglPemesanan;
	
	@ManyToOne
	@JoinColumn(name="supplier")
	private Supplier supplier;
	
	@ManyToOne
	@JoinColumn(name="obat")
	private Obat obat;
	
	@Column(name="jml_pemesanan")
	private Integer jmlPemesanan;
	
	@Column(name="status")
	private Integer status;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNoPemesanan() {
		return noPemesanan;
	}
	public void setNoPemesanan(String noPemesanan) {
		this.noPemesanan = noPemesanan;
	}
	public Date getTglPemesanan() {
		return tglPemesanan;
	}
	public void setTglPemesanan(Date tglPemesanan) {
		this.tglPemesanan = tglPemesanan;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	public Obat getObat() {
		return obat;
	}
	public void setObat(Obat obat) {
		this.obat = obat;
	}
	public Integer getJmlPemesanan() {
		return jmlPemesanan;
	}
	public void setJmlPemesanan(Integer jmlPemesanan) {
		this.jmlPemesanan = jmlPemesanan;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

}
