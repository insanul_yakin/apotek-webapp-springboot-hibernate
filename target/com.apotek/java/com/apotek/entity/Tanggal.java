package com.apotek.entity;

import javax.persistence.Entity;
import java.util.Date;

@Entity
public class Tanggal {
	private Date tgl_awal;
	private Date tgl_akhir;
	public Date getTgl_awal() {
		return tgl_awal;
	}
	public void setTgl_awal(Date tgl_awal) {
		this.tgl_awal = tgl_awal;
	}
	public Date getTgl_akhir() {
		return tgl_akhir;
	}
	public void setTgl_akhir(Date tgl_akhir) {
		this.tgl_akhir = tgl_akhir;
	}

}
