package com.apotek.entity;

import javax.persistence.*;

@Entity
@Table(name = "obat")
public class Obat {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@Column(name = "kode")
	private String kode;
	
	@ManyToOne
	@JoinColumn(name = "tipeobat")
	private TipeObat tipeObat;
	
	@Column(name = "nama_obat")
	private String namaObat;
	
	@ManyToOne
	@JoinColumn(name = "jenis_obat")
	private JenisObat jenisObat;
	
	@ManyToOne
	@JoinColumn(name = "satuan_obat")
	private SatuanObat satuanObat;
	
	@Column(name = "hargaBeli")
	private String hargaBeli;
	
	@Column(name = "hargaJual")
	private String hargaJual;
	
	@Column(name = "stok_obat")
	private Integer stok;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getKode() {
		return kode;
	}
	public void setKode(String kode) {
		this.kode = kode;
	}
	public TipeObat getTipeObat() {
		return tipeObat;
	}
	public void setTipeObat(TipeObat tipeObat) {
		this.tipeObat = tipeObat;
	}
	public String getNamaObat() {
		return namaObat;
	}
	public void setNamaObat(String namaObat) {
		this.namaObat = namaObat;
	}
	public JenisObat getJenisObat() {
		return jenisObat;
	}
	public void setJenisObat(JenisObat jenisObat) {
		this.jenisObat = jenisObat;
	}
	public SatuanObat getSatuanObat() {
		return satuanObat;
	}
	public void setSatuanObat(SatuanObat satuanObat) {
		this.satuanObat = satuanObat;
	}
	public String getHargaBeli() {
		return hargaBeli;
	}
	public void setHargaBeli(String hargaBeli) {
		this.hargaBeli = hargaBeli;
	}
	public String getHargaJual() {
		return hargaJual;
	}
	public void setHargaJual(String hargaJual) {
		this.hargaJual = hargaJual;
	}
	public Integer getStok() {
		return stok;
	}
	public void setStok(Integer stok) {
		this.stok = stok;
	}

}
