package com.apotek.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="pemesanan_jtempo")
public class PemesananJTempo {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@Column(name="no_faktur")
	private String noFaktur;
	
	@Column(name="tgl_jtempo")
	private Date tglJTempo;
	
	@ManyToOne
	@JoinColumn(name="pemesanan")
	private Pemesanan pemesanan;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNoFaktur() {
		return noFaktur;
	}
	public void setNoFaktur(String noFaktur) {
		this.noFaktur = noFaktur;
	}

	public Date getTglJTempo() {
		return tglJTempo;
	}
	public void setTglJTempo(Date tglJTempo) {
		this.tglJTempo = tglJTempo;
	}
	public Pemesanan getPemesanan() {
		return pemesanan;
	}
	public void setPemesanan(Pemesanan pemesanan) {
		this.pemesanan = pemesanan;
	}

}
