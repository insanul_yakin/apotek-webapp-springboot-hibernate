package com.apotek.entity;

import javax.persistence.*;

@Entity
@Table(name = "tipeObat")
public class TipeObat {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@Column(name = "kode")
	private String kode;
	
	@Column(name = "tipe")
	private String tipe;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getKode() {
		return kode;
	}
	public void setKode(String kode) {
		this.kode = kode;
	}
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}

}
