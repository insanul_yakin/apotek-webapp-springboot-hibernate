package com.apotek.util;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.ui.ModelMap;

public class Check {
	private static String result;
	
	public static String user(ModelMap modelMap) {
		try {
			User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String name = user.getUsername();
			modelMap.put("user", name);
			result = name;
		} catch (Exception e) {
			e.printStackTrace();
			result = "kosong";
			// TODO: handle exception
		}
		
		return result;
	}

}
