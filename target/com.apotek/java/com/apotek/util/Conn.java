package com.apotek.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conn {
	private static Connection conn=null;
	
	public static Connection getConnection() {
		if(conn!=null)
			return conn;
		else{
			try{
				Class.forName("org.postgresql.Driver").newInstance();
				conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/d_apotek","postgres","postgres");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return conn;
		}
	}

}
