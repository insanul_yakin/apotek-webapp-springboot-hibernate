


package com.apotek.service;

import com.apotek.entity.Supplier;

import java.util.List;

public interface SupplierService {
	
	public void addSupplier(Supplier supplier);
	
	public void deleteSupplier(Supplier supplier);
	
	public List<Supplier> listSuppliers();
	
	public Supplier getById(int id);

}
