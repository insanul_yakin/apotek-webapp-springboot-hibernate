package com.apotek.service;

import com.apotek.entity.User;

import java.util.List;

public interface UserService {
	
	public void add(User user);
	
	public void delete(User user);
	
	public List<User> listUsers();
	
	public User getUserById(int id);

}
