package com.apotek.service;

import com.apotek.entity.Pemesanan;

import java.util.List;

public interface PemesananService {
	
	public void add(Pemesanan pemesanan);
	
	public List<Pemesanan> listPemesanan();
	
	public Pemesanan getById(int id);
	
	public void delete(Pemesanan pemesanan);

}
