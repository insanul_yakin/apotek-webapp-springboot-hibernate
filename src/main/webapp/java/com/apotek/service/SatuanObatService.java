package com.apotek.service;

import com.apotek.entity.SatuanObat;

import java.util.List;

public interface SatuanObatService {
	
	public void add(SatuanObat satuanObat);
	
	public List<SatuanObat> listSatuanObat();
	
	public SatuanObat getById(int id);
	
	public void del(SatuanObat satuanObat);

}
