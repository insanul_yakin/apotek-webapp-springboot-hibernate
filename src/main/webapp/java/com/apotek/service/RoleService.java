package com.apotek.service;

import com.apotek.entity.Role;

import java.util.List;

public interface RoleService {
	
	public void create(Role role);
	
	public void delete(Role role);
	
	public List<Role> listRole();
	
	public Role getById(int id);
	
}
