package com.apotek.entity;

import javax.persistence.*;

@Entity
@Table(name = "supplier")
public class Supplier {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@Column(name="nama_supplier")
	private String namaSupplier;
	
	@Column(name="alamat_supplier")
	private String alamatSupplier;
	
	@Column(name="kota_supplier")
	private String kotaSupplier;
	
	@Column(name="telepon_supplier")
	private String tlpSupplier;
	
	@Column(name="email")
	private String email;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNamaSupplier() {
		return namaSupplier;
	}
	public void setNamaSupplier(String namaSupplier) {
		this.namaSupplier = namaSupplier;
	}
	public String getAlamatSupplier() {
		return alamatSupplier;
	}
	public void setAlamatSupplier(String alamatSupplier) {
		this.alamatSupplier = alamatSupplier;
	}
	public String getKotaSupplier() {
		return kotaSupplier;
	}
	public void setKotaSupplier(String kotaSupplier) {
		this.kotaSupplier = kotaSupplier;
	}
	public String getTlpSupplier() {
		return tlpSupplier;
	}
	public void setTlpSupplier(String tlpSupplier) {
		this.tlpSupplier = tlpSupplier;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

}
