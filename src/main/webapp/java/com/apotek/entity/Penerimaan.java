package com.apotek.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="penerimaan")
public class Penerimaan {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="pemesanan")
	private Pemesanan pemesanan;
	
	@Column(name="tgl_terima")
	private Date tglTerima;
	
	@Column(name="jml_terima")
	private String jmlTerima;
	
	@Column(name="harga_satuan")
	private String hargaSatuan;
	
	@Column(name="total_harga")
	private Double totalHarga;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Pemesanan getPemesanan() {
		return pemesanan;
	}
	public void setPemesanan(Pemesanan pemesanan) {
		this.pemesanan = pemesanan;
	}
	public Date getTglTerima() {
		return tglTerima;
	}
	public void setTglTerima(Date tglTerima) {
		this.tglTerima = tglTerima;
	}
	public String getJmlTerima() {
		return jmlTerima;
	}
	public void setJmlTerima(String jmlTerima) {
		this.jmlTerima = jmlTerima;
	}
	public String getHargaSatuan() {
		return hargaSatuan;
	}
	public void setHargaSatuan(String hargaSatuan) {
		this.hargaSatuan = hargaSatuan;
	}
	public Double getTotalHarga() {
		return totalHarga;
	}
	public void setTotalHarga(Double totalHarga) {
		this.totalHarga = totalHarga;
	}

}
