package com.apotek.entity;

import javax.persistence.*;

@Entity
@Table(name="detil_penjualan")
public class DetilPenjualan {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="penjualan")
	private Penjualan penjualan;
	
	@ManyToOne
	@JoinColumn(name="obat")
	private Obat obat;
	
	@Column(name="jml_beli")
	private Integer jmlBeli;
	
	@Column(name="total_harga")
	private Double totalHarga;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Penjualan getPenjualan() {
		return penjualan;
	}
	public void setPenjualan(Penjualan penjualan) {
		this.penjualan = penjualan;
	}
	public Obat getObat() {
		return obat;
	}
	public void setObat(Obat obat) {
		this.obat = obat;
	}
	public Integer getJmlBeli() {
		return jmlBeli;
	}
	public void setJmlBeli(Integer jmlBeli) {
		this.jmlBeli = jmlBeli;
	}
	public Double getTotalHarga() {
		return totalHarga;
	}
	public void setTotalHarga(Double totalHarga) {
		this.totalHarga = totalHarga;
	}

}
