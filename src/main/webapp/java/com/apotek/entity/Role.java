package com.apotek.entity;

import javax.persistence.*;

@Entity
@Table(name="role")
public class Role {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@Column(name="authority")
	private String authority;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}

}
