package com.apotek.entity;

import javax.persistence.*;

@Entity
@Table(name="jenisObat")
public class JenisObat {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@Column(name = "kode")
	private String kode;
	
	@Column(name = "jenis")
	private String jenis;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getKode() {
		return kode;
	}
	public void setKode(String kode) {
		this.kode = kode;
	}
	public String getJenis() {
		return jenis;
	}
	public void setJenis(String jenis) {
		this.jenis = jenis;
	}

}
