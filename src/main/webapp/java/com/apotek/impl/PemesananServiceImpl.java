package com.apotek.impl;

import com.apotek.entity.Pemesanan;
import com.apotek.service.PemesananService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("pemesananService")
@Transactional
public class PemesananServiceImpl implements PemesananService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void add(Pemesanan pemesanan) {
		sessionFactory.getCurrentSession().saveOrUpdate(pemesanan);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Pemesanan> listPemesanan() {
		return sessionFactory.getCurrentSession().createQuery("From Pemesanan").list();
	}
	
	@Override
	public Pemesanan getById(int id) {
		return (Pemesanan) sessionFactory.getCurrentSession().get(Pemesanan.class, id);
	}
	
	@Override
	public void delete(Pemesanan pemesanan) {
		sessionFactory.getCurrentSession().delete(pemesanan);
	}

}
