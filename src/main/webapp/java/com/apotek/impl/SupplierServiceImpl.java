package com.apotek.impl;

import com.apotek.entity.Supplier;
import com.apotek.service.SupplierService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("supplierService")
@Transactional
public class SupplierServiceImpl implements SupplierService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addSupplier(Supplier supplier) {
		sessionFactory.getCurrentSession().saveOrUpdate(supplier);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Supplier> listSuppliers() {
		return sessionFactory.getCurrentSession().createQuery("From Supplier").list();
	}
	
	@Override
	public void deleteSupplier(Supplier supplier) {
		sessionFactory.getCurrentSession().delete(supplier);
	}
	
	@Override
	public Supplier getById(int id) {
		return (Supplier) sessionFactory.getCurrentSession().get(Supplier.class, id);
	}

}
