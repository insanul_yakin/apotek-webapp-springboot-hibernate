package com.apotek.impl;

import com.apotek.entity.JenisObat;
import com.apotek.service.JenisObatService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("jenisObat")
@Transactional
public class JenisObatServiceImpl implements JenisObatService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override	
	public void add(JenisObat jenisObat) {
		sessionFactory.getCurrentSession().saveOrUpdate(jenisObat);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<JenisObat> listJenisObat() {
		return sessionFactory.getCurrentSession().createQuery("From JenisObat").list();
	}
	
	@Override
	public JenisObat getById(int id) {
		return (JenisObat) sessionFactory.getCurrentSession().get(JenisObat.class, id);
	}
	
	@Override
	public void delete(JenisObat jenisObat) {
		sessionFactory.getCurrentSession().delete(jenisObat);
	}

}
