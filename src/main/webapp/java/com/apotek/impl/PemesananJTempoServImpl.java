package com.apotek.impl;

import com.apotek.entity.PemesananJTempo;
import com.apotek.service.PemesananJTempoService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("pemesananJTempoService")
@Transactional
public class PemesananJTempoServImpl implements PemesananJTempoService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void add(PemesananJTempo jTempo) {
		sessionFactory.getCurrentSession().saveOrUpdate(jTempo);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PemesananJTempo> listPembelianJTempo() {
		return sessionFactory.getCurrentSession().createQuery("From PemesananJTempo").list();
	}
	
	@Override
	public PemesananJTempo getById(int id) {
		return (PemesananJTempo) sessionFactory.getCurrentSession().get(PemesananJTempo.class, id);
	}
	
	@Override
	public void del(PemesananJTempo jTempo) {
		sessionFactory.getCurrentSession().delete(jTempo);
	}

}
