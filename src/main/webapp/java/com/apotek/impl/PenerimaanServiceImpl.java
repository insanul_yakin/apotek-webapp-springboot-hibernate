package com.apotek.impl;

import com.apotek.entity.Penerimaan;
import com.apotek.service.PenerimaanService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("penerimaanService")
@Transactional
public class PenerimaanServiceImpl implements PenerimaanService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void add(Penerimaan penerimaan) {
		sessionFactory.getCurrentSession().saveOrUpdate(penerimaan);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Penerimaan> listPenerimaan() {
		return sessionFactory.getCurrentSession().createQuery("From Penerimaan").list();
	}
	
	@Override
	public Penerimaan getById(int id) {
		return (Penerimaan) sessionFactory.getCurrentSession().get(Penerimaan.class, id);
	}
	
	@Override
	public void del(Penerimaan penerimaan) {
		sessionFactory.getCurrentSession().delete(penerimaan);
	}

}
