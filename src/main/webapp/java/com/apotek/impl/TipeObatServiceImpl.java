package com.apotek.impl;

import com.apotek.entity.TipeObat;
import com.apotek.service.TipeObatService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("tipeObat")
@Transactional
public class TipeObatServiceImpl implements TipeObatService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void add(TipeObat tipeObat) {
		sessionFactory.getCurrentSession().saveOrUpdate(tipeObat);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TipeObat> listTipeObat() {
		return sessionFactory.getCurrentSession().createQuery("From TipeObat").list();
	}
	
	@Override
	public TipeObat getById(int id) {
		return (TipeObat) sessionFactory.getCurrentSession().get(TipeObat.class, id);
	}
	
	@Override
	public void del(TipeObat tipeObat) {
		sessionFactory.getCurrentSession().delete(tipeObat);
	}

}
