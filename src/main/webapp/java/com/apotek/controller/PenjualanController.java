package com.apotek.controller;

import com.apotek.entity.DetilPenjualan;
import com.apotek.entity.Obat;
import com.apotek.entity.Penjualan;
import com.apotek.entity.Tanggal;
import com.apotek.service.DPenjualanService;
import com.apotek.service.ObatService;
import com.apotek.service.PenjualanService;
import com.apotek.util.Conn;
import net.sf.jasperreports.engine.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "master/*")
public class PenjualanController {
	
	@Autowired
	private ObatService obatService;
	
	@Autowired
	private PenjualanService penjualanService;
	
	@Autowired
	private DPenjualanService dPenjualanService;
	
	@RequestMapping(value = "penjualan/list")
	public String list(ModelMap modelMap) {
		List<Penjualan> listPenjualan = penjualanService.listPenjualan();
		modelMap.put("listPenjualan", listPenjualan);
		return "/master/penjualan/list";
	}
	
	@RequestMapping(value = "penjualan/add", method = RequestMethod.GET)
	public String add(ModelMap modelMap) {
		List<Obat> obat = obatService.listObat();
		modelMap.put("obat", obat);
		
		return "/master/penjualan/add";
	}
	
	@RequestMapping(value = "penjualan/get", method = RequestMethod.POST)
	public @ResponseBody
    Obat get (@RequestParam String id) {
		int idObat = Integer.parseInt(id);
		Obat obat = obatService.getById(idObat);
		return obat;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "penjualan/add", method = RequestMethod.POST)
	public String save(@ModelAttribute Penjualan penjualan, HttpSession session) {
		List<DetilPenjualan> list = (List<DetilPenjualan>) session.getAttribute("cart");
		penjualanService.add(penjualan);
		for (DetilPenjualan dp : list) {
			dp.setPenjualan(penjualan);
			dPenjualanService.add(dp);
			
			Obat obat = obatService.getById(dp.getObat().getId());
			obat.setStok(obat.getStok() - dp.getJmlBeli());
			obatService.add(obat);
		}
		session.removeAttribute("cart");
		session.removeAttribute("total");
		return "redirect:list";
	}
	
	@RequestMapping(value = "penjualan/report", method = RequestMethod.GET)
	public String report() {
		return "/master/penjualan/report";
	}
	
	@RequestMapping(value = "penjualan/report", method = RequestMethod.POST)
	public void cetak(@ModelAttribute Tanggal tgl, HttpServletResponse response, HttpSession session) throws JRException, IOException {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tgl_awal", tgl.getTgl_awal());
		param.put("tgl_akhir", tgl.getTgl_akhir());
		
		File file = new File(session.getServletContext().getRealPath("/reports/LaporanPenjualan.jasper"));
		byte[] bs = JasperRunManager.runReportToPdf(file.getPath(), param, Conn.getConnection());
		
		response.setContentType("application/pdf");
		response.setContentLength(bs.length);
		ServletOutputStream outputStream = response.getOutputStream();
		outputStream.write(bs, 0, bs.length);
//		outputStream.flush();
//		outputStream.close();
	}
	
	@RequestMapping(value = "penjualan/retur", method = RequestMethod.GET)
	public void retur(@ModelAttribute Penjualan penjualan, HttpServletResponse response, HttpSession session) throws JRException, IOException {
		//InputStream jasperStream = this.getClass().getResourceAsStream("/reports/ReturPenjualan.jasper");
		File jasperStream = new File(session.getServletContext().getRealPath("/reports/ReturPenjualan.jasper"));
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", penjualan.getId());
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperStream.getPath(), params, Conn.getConnection());
		response.setContentType("application/pdf");
		response.setHeader("Content-disposition", "inline; filename=retur.pdf");
		OutputStream outputStream = response.getOutputStream();
		JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
	}

}

/*Japser Download*/
/*InputStream	 jasperStream = this.getClass().getResourceAsStream("/reports/LaporanPemesanan.jasper");
File file = new File(session.getServletContext().getRealPath("/reports/LaporanPemesanan.jasper"));
Map<String, Object> params = new HashMap<String, Object>();
params.put("tgl_awal", tgl.getTgl_awal());
params.put("tgl_akhir", tgl.getTgl_akhir());
JasperPrint jasperPrint = JasperFillManager.fillReport(file.getPath(), params, Conn.getConnection());

response.setContentType("application/x-pdf");
response.setHeader("Content-disposition", "inline; filename=helloWorldReport.pdf");

OutputStream outputStream = response.getOutputStream();
JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);*/
