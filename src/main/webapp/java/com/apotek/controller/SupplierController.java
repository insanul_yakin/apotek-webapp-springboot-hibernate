package com.apotek.controller;

import com.apotek.entity.Supplier;
import com.apotek.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping(value = "master/*")
public class SupplierController {
	
	@Autowired
	private SupplierService supplierService;
	
	@RequestMapping(value = "supplier/list")
	public String list(ModelMap modelMap) {
		List<Supplier> suppliers = supplierService.listSuppliers();
		modelMap.put("suppliers", suppliers);
		return "/master/supplier/list";
	}
	
	@RequestMapping(value = "supplier/add", method = RequestMethod.GET)
	public String add() {
		return "/master/supplier/add";
	}
	
	@RequestMapping(value = "supplier/add", method = RequestMethod.POST)
	public String save(@ModelAttribute Supplier supplier, BindingResult result) {
		supplierService.addSupplier(supplier);
		return "redirect:list";
	}
	
	@RequestMapping(value = "supplier/edit", method = RequestMethod.GET)
	public String edit(@ModelAttribute Supplier supplier, ModelMap modelMap) {
		supplier = supplierService.getById(supplier.getId());
		modelMap.put("supplier", supplier);
		return "/master/supplier/detail";
	}
	
	@RequestMapping(value = "supplier/delete", method = RequestMethod.GET)
	public String delete(@ModelAttribute Supplier supplier) {
		supplierService.deleteSupplier(supplier);
		return "redirect:list";
	}

}
