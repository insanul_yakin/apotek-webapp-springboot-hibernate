package com.apotek.controller;

import com.apotek.entity.TipeObat;
import com.apotek.service.TipeObatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "master/*")
public class TipeObatController {
	
	@Autowired
	private TipeObatService tipeObatService;
	
	@RequestMapping(value = "obat/tipe", method = RequestMethod.GET)
	public String tipe(ModelMap modelMap) {
		List<TipeObat> tipeObat = tipeObatService.listTipeObat();
		modelMap.put("tipeObat", tipeObat);
		return "/master/obat/tipe-obat";
	}
	
	@RequestMapping(value = "obat/tipe", method = RequestMethod.POST)
	public String save(@ModelAttribute TipeObat tipeObat) {
		tipeObatService.add(tipeObat);
		return "redirect:tipe";
	}
	
	@RequestMapping(value = "obat/deleteTipe", method = RequestMethod.GET)
	public String delete(@ModelAttribute TipeObat tipeObat) {
		tipeObatService.del(tipeObat);
		return "redirect:tipe";
	}
	
	@RequestMapping(value = "obat/editTipe", method = RequestMethod.POST)
	public @ResponseBody
    TipeObat edit(@RequestParam String name) {
		int id = Integer.parseInt(name);
		TipeObat tipeObat = tipeObatService.getById(id);
		return tipeObat;
	}

}
