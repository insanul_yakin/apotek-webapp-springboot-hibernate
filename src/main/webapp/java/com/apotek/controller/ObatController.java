package com.apotek.controller;

import com.apotek.entity.JenisObat;
import com.apotek.entity.Obat;
import com.apotek.entity.SatuanObat;
import com.apotek.entity.TipeObat;
import com.apotek.service.JenisObatService;
import com.apotek.service.ObatService;
import com.apotek.service.SatuanObatService;
import com.apotek.service.TipeObatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping(value = "master/*")
public class ObatController {
	
	@Autowired
	private ObatService obatService;
	
	@Autowired
	private TipeObatService tipeObatService;
	
	@Autowired
	private JenisObatService jenisObatService;
	
	@Autowired
	private SatuanObatService satuanObatService;
	
	@RequestMapping(value = "obat/list")
	public String list(ModelMap modelMap) {
		List<Obat> obat = obatService.listObat();
		modelMap.put("obat", obat);
		return "/master/obat/list";
	}
	
	@RequestMapping(value = "obat/add", method = RequestMethod.GET)
	public String add(ModelMap modelMap) {
		List<TipeObat> tipeObat = tipeObatService.listTipeObat();
		modelMap.put("tipeObat", tipeObat);
		
		List<JenisObat> jenisObat = jenisObatService.listJenisObat();
		modelMap.put("jenisObat", jenisObat);
		
		List<SatuanObat> satuanObat = satuanObatService.listSatuanObat();
		modelMap.put("satuanObat", satuanObat);
		return "/master/obat/add";
	}
	
	@RequestMapping(value = "obat/add", method = RequestMethod.POST)
	public String save(@ModelAttribute Obat obat, BindingResult result) {
		int idTipe = Integer.parseInt(result.getFieldValue("tipeObat").toString());
		TipeObat tipeObat = tipeObatService.getById(idTipe);
		
		int idJenis = Integer.parseInt(result.getFieldValue("jenisObat").toString());
		JenisObat jenisObat = jenisObatService.getById(idJenis);
		
		int idSatuan = Integer.parseInt(result.getFieldValue("satuanObat").toString());
		SatuanObat satuanObat = satuanObatService.getById(idSatuan);
		
		obat.setTipeObat(tipeObat);
		obat.setJenisObat(jenisObat);
		obat.setSatuanObat(satuanObat);
		obatService.add(obat);
		return "redirect:list";
	}
	
	@RequestMapping(value = "obat/edit", method = RequestMethod.GET)
	public String edit(@ModelAttribute Obat obat, ModelMap modelMap) {
		obat = obatService.getById(obat.getId());
		modelMap.put("obat", obat);
		
		List<TipeObat> tipeObat = tipeObatService.listTipeObat();
		modelMap.put("tipeObat", tipeObat);
		
		List<JenisObat> jenisObat = jenisObatService.listJenisObat();
		modelMap.put("jenisObat", jenisObat);
		
		List<SatuanObat> satuanObat = satuanObatService.listSatuanObat();
		modelMap.put("satuanObat", satuanObat);
		
		return "/master/obat/edit";
	}
	
	@RequestMapping(value = "obat/delete", method = RequestMethod.GET)
	public String delete(@ModelAttribute Obat obat) {
		obatService.delete(obat);
		return "redirect:list";
	}

}
