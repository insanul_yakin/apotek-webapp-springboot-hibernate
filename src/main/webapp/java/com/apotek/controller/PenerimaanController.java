package com.apotek.controller;

import com.apotek.entity.Obat;
import com.apotek.entity.Pemesanan;
import com.apotek.entity.Penerimaan;
import com.apotek.entity.Tanggal;
import com.apotek.service.ObatService;
import com.apotek.service.PemesananService;
import com.apotek.service.PenerimaanService;
import com.apotek.util.Conn;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value="master/*")
public class PenerimaanController {
	
	@Autowired
	private PemesananService pemesananService;
	
	@Autowired
	private PenerimaanService penerimaanService;
	
	@Autowired
	private ObatService obatService;
	
	@RequestMapping(value="penerimaan/list")
	public String list(ModelMap modelMap) {
		List<Penerimaan> penerimaan = penerimaanService.listPenerimaan();
		modelMap.put("penerimaan", penerimaan);
		
		return "/master/penerimaan/list";
	}
	
	@RequestMapping(value="penerimaan/add", method = RequestMethod.GET)
	public String add(ModelMap modelMap) {
		List<Pemesanan> pemesanan = pemesananService.listPemesanan();
		modelMap.put("pemesanan", pemesanan);
		return "/master/penerimaan/add";
	}
	
	@RequestMapping(value="penerimaan/add", method = RequestMethod.POST)
	public String save(@ModelAttribute Penerimaan penerimaan, BindingResult result) throws ParseException {
		int idPemesanan = Integer.parseInt(result.getFieldValue("pemesanan").toString());
		Pemesanan pemesanan = pemesananService.getById(idPemesanan);
		
		penerimaan.setPemesanan(pemesanan);
		penerimaanService.add(penerimaan);
		
		Obat obat = pemesanan.getObat();
		int stokObat = obat.getStok();
		int jmlTerima = Integer.parseInt(penerimaan.getJmlTerima());
		int stok = stokObat + jmlTerima;
		obat.setStok(stok);
		obatService.add(obat);
		
		pemesanan.setStatus(1);
		pemesananService.add(pemesanan);
		return "redirect:list";
	}
	
	@RequestMapping(value = "penerimaan/edit", method = RequestMethod.GET)
	public String detail(@ModelAttribute Penerimaan penerimaan, ModelMap map) {
		List<Pemesanan> pemesanan = pemesananService.listPemesanan();
		map.put("pesan", pemesanan);
		
		penerimaan = penerimaanService.getById(penerimaan.getId());
		map.put("terima", penerimaan);
		return "/master/penerimaan/detail";
	}
	
	@RequestMapping(value = "penerimaan/delete", method = RequestMethod.GET)
	public String del(@ModelAttribute Penerimaan penerimaan) {
		penerimaanService.del(penerimaan);
		return "redirect:list";
	}
	
	@RequestMapping(value = "penerimaan/report", method = RequestMethod.GET)
	public String report() {
		return "/master/penerimaan/report";
	}
	
	@RequestMapping(value = "penerimaan/report", method = RequestMethod.POST)
	public void cetak(@ModelAttribute Tanggal tgl, HttpServletResponse response, HttpSession session) throws JRException, IOException {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tgl_awal", tgl.getTgl_awal());
		param.put("tgl_akhir", tgl.getTgl_akhir());
		
		File file = new File(session.getServletContext().getRealPath("/reports/LaporanPenerimaan.jasper"));
		byte[] bs = JasperRunManager.runReportToPdf(file.getPath(), param, Conn.getConnection());
		
		response.setContentType("application/pdf");
		response.setContentLength(bs.length);
		ServletOutputStream outputStream = response.getOutputStream();
		outputStream.write(bs, 0, bs.length);
		outputStream.flush();
		outputStream.close();
	}
	
	@RequestMapping(value = "penerimaan/get", method = RequestMethod.POST)
	public @ResponseBody
    Pemesanan getPenerimaan(@RequestParam String id) {
		int idPenerima = Integer.parseInt(id);
		Pemesanan pemesanan = pemesananService.getById(idPenerima);
		return pemesanan;
	}

}
