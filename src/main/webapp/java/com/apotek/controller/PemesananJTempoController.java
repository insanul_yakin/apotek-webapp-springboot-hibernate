package com.apotek.controller;

import com.apotek.entity.Pemesanan;
import com.apotek.entity.PemesananJTempo;
import com.apotek.entity.Tanggal;
import com.apotek.service.PemesananJTempoService;
import com.apotek.service.PemesananService;
import com.apotek.util.Conn;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "master/pemesananJTempo/*")
public class PemesananJTempoController {
	
	@Autowired
	private PemesananJTempoService jTempoService;
	
	@Autowired
	private PemesananService pemesananService;
	
	@RequestMapping(value = "list")
	public String list(ModelMap modelMap) {
		List<PemesananJTempo> jTempo = jTempoService.listPembelianJTempo();
		modelMap.put("jTempo", jTempo);
		return "/master/pemesananJTempo/list";
	}
	
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String add(ModelMap modelMap) {
		List<Pemesanan> list = pemesananService.listPemesanan();
		modelMap.put("pemesanan", list);
		return "/master/pemesananJTempo/add";
	}
	
	@RequestMapping(value = "add", method = RequestMethod.POST)
	public String save(@ModelAttribute PemesananJTempo jTempo, BindingResult result) {
		int idPesan = Integer.parseInt(result.getFieldValue("pemesanan").toString());
		Pemesanan pemesanan = pemesananService.getById(idPesan);
		pemesanan.setStatus(1);
		pemesananService.add(pemesanan);
		
		jTempo.setPemesanan(pemesanan);
		jTempoService.add(jTempo);
		return "redirect:list";
	}
	
	@RequestMapping(value = "edit", method= RequestMethod.GET)
	public String edit(@ModelAttribute PemesananJTempo jTempo, ModelMap modelMap) {
		List<Pemesanan> pemesanan = pemesananService.listPemesanan();
		modelMap.put("pemesanan", pemesanan);
		
		jTempo = jTempoService.getById(jTempo.getId());
		modelMap.put("jTempo", jTempo);
		return "/master/pemesananJTempo/detail";
	}
	
	@RequestMapping(value = "delete", method = RequestMethod.GET)
	public String del(@ModelAttribute PemesananJTempo jTempo) {
		jTempoService.del(jTempo);
		return "redirect:list";
	}
	
	@RequestMapping(value = "report", method = RequestMethod.GET)
	public String report() {
		return "/master/pemesananJTempo/report";
	}
	
	@RequestMapping(value = "report", method = RequestMethod.POST)
	public void cetak(@ModelAttribute Tanggal tgl, HttpServletResponse response, HttpSession session) throws JRException, IOException {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tgl_awal", tgl.getTgl_awal());
		param.put("tgl_akhir", tgl.getTgl_akhir());
		
		File file = new File(session.getServletContext().getRealPath("/reports/LaporanPemesananJTempo.jasper"));
		byte[] bs = JasperRunManager.runReportToPdf(file.getPath(), param, Conn.getConnection());
		
		response.setContentType("application/pdf");
		response.setContentLength(bs.length);
		ServletOutputStream outputStream = response.getOutputStream();
		outputStream.write(bs, 0, bs.length);
		outputStream.flush();
		outputStream.close();
	}
	
	@RequestMapping(value = "get", method = RequestMethod.POST)
	public @ResponseBody
    Pemesanan get(@RequestParam String id) {
		int idPesan = Integer.parseInt(id);
		Pemesanan pemesanan = pemesananService.getById(idPesan);
		return pemesanan;
	}

}
