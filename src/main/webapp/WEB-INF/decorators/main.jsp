<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
   
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title><decorator:title></decorator:title></title>
	<link href='<c:url value="/resources/css/bootstrap.min.css"/>' rel='stylesheet'>
	<link href='<c:url value="/resources/css/style.css"/>' rel='stylesheet'>
	<link href='<c:url value="/resources/css/font-awesome.css"/>' rel='stylesheet'>
	<link href='<c:url value="/resources/css/icon-font.min.css"/>' rel='stylesheet'>
	<link href='<c:url value="/resources/css/animate.css"/>' rel='stylesheet'>
	
	<link href='<c:url value="/resources/images/apotek.png"/>' rel='icon'>
	
	<script src="<c:url value='/resources/js/Chart.js'/>"></script>
	<script src="<c:url value='/resources/js/wow.min.js'/>"></script>
	<script>
		 new WOW().init();
	</script>
	<script src="<c:url value='/resources/js/jquery-1.10.2.min.js'/>"></script>
	<decorator:head></decorator:head>
</head>
<body class="sticky-header left-side-collapsed">
	<section>
		<div class="left-side sticky-left-side">
			
			<!--logo and iconic logo start-->
			<div class="logo">
				<h1><a href="">Easy Admin</a></h1>
			</div>
			<div class="logo-icon text-center">
				<a href=""><i class="lnr lnr-home"></i> </a>
			</div>
			
			<!--logo and iconic logo end-->
			<div class="left-side-inner">
				
				<!--sidebar nav start-->
				<ul class="nav nav-pills nav-stacked custom-nav">
					<li class="active"><a href="<c:url value='/master/home'/>"><i class="lnr lnr-power-switch"></i><span>Dashboard</span></a></li>
					<li class="menu-list">
						<a href="#"><i class="lnr lnr-menu"></i><span>Data</span></a>
						<ul class="sub-menu-list">
							<li><a href="<c:url value='/master/user/list'/>">Data User</a></li>
							<li><a href="<c:url value='/master/supplier/list'/>">Data Supplier</a></li>
							<li><a href="<c:url value='/master/obat/list'/>">Data Obat</a></li>
						</ul>
					</li>
					<li class="menu-list">
						<a href="#"><i class="lnr lnr-spell-check"></i><span>Transaksi</span></a>
						<ul class="sub-menu-list">
							<li><a href="<c:url value='/master/pemesanan/add'/>">Pemesanan Obat</a> </li>
							<li><a href="<c:url value='/master/penerimaan/add'/>">Penerimaan Obat</a></li>
							<li><a href="<c:url value='/master/penjualan/add'/>">Penjualan Obat</a></li>
							<li><a href="<c:url value='/master/pemesananJTempo/add'/>">Pemesanan Jatuh Tempo</a></li>
						</ul>
					</li>
					<li class="menu-list">
						<a href="#"><i class="lnr lnr-list"></i><span>List</span></a>
						<ul class="sub-menu-list">
							<li><a href="<c:url value='/master/pemesanan/list'/>">Pemesanan Obat</a> </li>
							<li><a href="<c:url value='/master/penerimaan/list'/>">Penerimaan Obat</a></li>
							<li><a href="<c:url value='/master/penjualan/list'/>">Penjualan Obat</a></li>
							<li><a href="<c:url value='/master/pemesananJTempo/list'/>">Pemesanan Jatuh Tempo</a></li>
						</ul>
					</li>
					<li class="menu-list">
						<a href="#"><i class="lnr lnr-pencil"></i><span>Laporan</span></a>
						<ul class="sub-menu-list">
							<li><a href="<c:url value='/master/pemesanan/report'/>">Laporan Pemesanan</a> </li>
							<li><a href="<c:url value='/master/penerimaan/report'/>">Laporan Penerimaan</a></li>
							<li><a href="<c:url value='/master/penjualan/report'/>">Laporan Penjualan</a></li>
							<li><a href="<c:url value='/master/pemesananJTempo/report'/>">Laporan Pemesanan Jatuh Tempo</a></li>
						</ul>
					</li>
				</ul>
				
			</div>
			
		</div>
		
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<div class="header-section">
				
				<!--toggle button start-->
				<a class="toggle-btn  menu-collapsed"><i class="fa fa-bars"></i></a>
				<!--toggle button end-->
				
				<!--notification menu start -->
				<div class="menu-right">
					<div class="user-panel-top">  	
						<div class="profile_details_left">
							<ul class="nofitications-dropdown">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope"></i><span class="badge">3</span></a>
									<ul class="dropdown-menu">
										<li>
											<div class="notification_header">
												<h3>You have 3 new messages</h3>
											</div>
										</li>
										<li><a href="#">
											<div class="user_img"><img src="../resources/images/1.png" alt=""></div>
											<div class="notification_desc">
												<p>Lorem ipsum dolor sit amet</p>
												<p><span>1 hour ago</span></p>
											</div>
											<div class="clearfix"></div>
										</a></li>
										<li class="odd"><a href="#">
											<div class="user_img"><img src="../resources/images/1.png" alt=""></div>
											<div class="notification_desc">
												<p>Lorem ipsum dolor sit amet </p>
												<p><span>1 hour ago</span></p>
											</div>
											<div class="clearfix"></div>
										</a></li>
										<li><a href="#">
											<div class="user_img"><img src="../resources/images/1.png" alt=""></div>
											<div class="notification_desc">
												<p>Lorem ipsum dolor sit amet </p>
												<p><span>1 hour ago</span></p>
											</div>
											<div class="clearfix"></div>
										</a></li>
										<li>
											<div class="notification_bottom">
												<a href="#">See all messages</a>
											</div>
										</li>
									</ul>
								</li>
							</ul>
						</div>
						<div class="profile_details">
							<ul>
								<li class="dropdown profile_details_drop">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										<div class="profile_img">	
											<span style="background:url(<c:url value='/resources/images/1.jpg'></c:url>) no-repeat center"> </span>
										 	<div class="user-name">
												<p>hahaha<span>Administrator</span></p>
										 	</div>
										 	<i class="lnr lnr-chevron-down"></i>
										 	<i class="lnr lnr-chevron-up"></i>
											<div class="clearfix"></div>	
										</div>	
									</a>
									<ul class="dropdown-menu drp-mnu">
										<li> <a href="#"><i class="fa fa-cog"></i> Settings</a> </li> 
										<li> <a href="#"><i class="fa fa-user"></i>Profile</a> </li> 
										<li> <a href="<c:url value="/j_spring_security_logout" />"><i class="fa fa-sign-out"></i> Logout</a> </li>
									</ul>
								</li>
								<div class="clearfix"> </div>
							</ul>
						</div>	
					</div>
				</div>
			
			</div>
			
			<div id="page-wrapper">
				<decorator:body></decorator:body>
			</div>
			
		</div>
		
<!-- 		<footer> -->
<!-- 			<p>&copy 2016 Easy Admin Panel. All Rights Reserved | Design by <a href="https://w3layouts.com/" target="_blank">w3layouts.</a></p> -->
<!-- 		</footer> -->
	</section>
	
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.nicescroll.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/scripts.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>
</body>
</html>