<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="<c:url value="/master/home"/>">Home</a></li>
		<li><a href="<c:url value="/master/obat/list"/>">Data Obat</a></li>
		<li class="active">Edit</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4">
			<div class="graphs">
				<h3 class="blank1">Form Tambah Supplier</h3>
				<div class="tab-content">
					<div class="tab-pane active" id="horizontal-form">
						<form class="form-horizontal" action="add" method="post">
							<div class="form-group">
								<label class="col-sm-2 control-label">ID</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="id" value="${obat.id }" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Kode Obat</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="kode" value="${obat.kode }">
								</div>
							</div>
							<div class="form-group">
								<label for="selector1" class="col-sm-2 control-label">Tipe Obat</label>
								<div class="col-sm-5">
									<select name="tipeObat" class="form-control1">
										<option value="${obat.tipeObat.id }">${obat.tipeObat.tipe }</option>
										<c:forEach items="${tipeObat }" var="tipe">
											<c:if test="${tipe.id != obat.tipeObat.id }">
												<option value="${tipe.id }">${tipe.tipe }</option>
											</c:if>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Obat</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="namaObat" value="${obat.namaObat }">
								</div>
							</div>
							<div class="form-group">
								<label for="selector1" class="col-sm-2 control-label">Jenis Obat</label>
								<div class="col-sm-5">
									<select name="jenisObat" class="form-control1">
										<option value="${obat.jenisObat.id }">${obat.jenisObat.jenis }</option>
										<c:forEach items="${jenisObat }" var="jenis">
											<c:if test="${jenis.id != obat.jenisObat.id }">
												<option value="${jenis.id }">${jenis.jenis }</option>
											</c:if>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="selector1" class="col-sm-2 control-label">Satuan</label>
								<div class="col-sm-5">
									<select name="satuanObat" class="form-control1">
										<option value="${obat.satuanObat.id }">${obat.satuanObat.satuan }</option>
										<c:forEach items="${satuanObat }" var="satuan">
											<c:if test="${satuan.id != obat.satuanObat.id }">
												<option value="${satuan.id }">${satuan.satuan }</option>
											</c:if>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Harga Beli</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="hargaBeli" value="${obat.hargaBeli }">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Harga Jual</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="hargaJual" value="${obat.hargaJual }">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Stok Obat</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="stok" value="${obat.stok }">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-5">
									<button type="submit" class="btn btn-custom btn-sm">Save</button>
									<button type="reset" class="btn btn-custom btn-sm">Reset</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>