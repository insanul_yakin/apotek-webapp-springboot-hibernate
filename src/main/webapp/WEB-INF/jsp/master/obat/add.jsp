<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Data Obat | add</title>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="<c:url value="/master/home"/>">Home</a></li>
		<li><a href="<c:url value="/master/obat/list"/>">Data Obat</a></li>
		<li class="active">Tambah Obat</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4">
			<div class="graphs">
				<h3 class="blank1">Form Tambah Obat</h3>
				<div class="tab-content">
					<div class="tab-pane active" id="horizontal-form">
						<form class="form-horizontal" action="add" method="post" id="formObat">
							<div class="form-group">
								<label class="col-sm-2 control-label">Kode Obat</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="kode" placeholder="Kode Obat">
								</div>
							</div>
							<div class="form-group">
								<label for="selector1" class="col-sm-2 control-label">Tipe Obat</label>
								<div class="col-sm-5">
									<select name="tipeObat" class="form-control1">
										<option value=" ">-- Tipe Obat --</option>
										<c:forEach items="${tipeObat }" var="tipe">
											<option value="${tipe.id }">${tipe.tipe }</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Obat</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="namaObat" placeholder="Nama Obat">
								</div>
							</div>
							<div class="form-group">
								<label for="selector1" class="col-sm-2 control-label">Jenis Obat</label>
								<div class="col-sm-5">
									<select name="jenisObat" class="form-control1">
										<option value="">-- Jenis Obat --</option>
										<c:forEach items="${jenisObat }" var="jenis">
											<option value="${jenis.id }">${jenis.jenis }</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="selector1" class="col-sm-2 control-label">Satuan</label>
								<div class="col-sm-5">
									<select name="satuanObat" class="form-control1">
										<option value="">-- Satuan Obat --</option>
										<c:forEach items="${satuanObat }" var="satuan">
											<option value="${satuan.id }">${satuan.satuan }</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Harga Beli</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="hargaBeli" placeholder="Harga Beli">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Harga Jual</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="hargaJual" placeholder="Harga Jual">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Stok Obat</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="stok" placeholder="Stok Obat">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-5">
									<button type="submit" class="btn btn-custom btn-sm">Save</button>
									<button type="reset" class="btn btn-custom btn-sm">Reset</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<script src="<c:url value='/resources/js/bootstrapValidator.js'/>"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#formObat').bootstrapValidator({
				container: 'tooltip',
	    		feedbackIcons: {
	                valid: ' ',
	                invalid: 'glyphicon glyphicon-remove',
	                validating: 'glyphicon glyphicon-refresh'
	            },
				fields: {
					kode: {
						validators: {
							notEmpty: {
		                        message: 'kode tidak boleh kosong'
		                    },
						}
					},
					tipeObat: {
						validators: {
							notEmpty: {
								message: 'tipe obat belum dipilih'
							}
						}
					},
					namaObat: {
						validators: {
							notEmpty: {
								message: 'nama obat tidak boleh kosong'
							}
						}
					},
					jenisObat: {
						validators: {
							notEmpty: {
								message: 'jenis obat belum dipilih'
							}
						}
					},
					satuanObat: {
						validators: {
							notEmpty: {
								message: 'satuan obat belum dipilih'
							}
						}
					},
					hargaBeli: {
						validators: {
							notEmpty: {
								message: 'masukkan harga pembelian obat'
							}
						}
					},
					hargaJual: {
						validators: {
							notEmpty: {
								message: 'masukkan harga penjualan obat'
							}
						}
					},
					stok: {
						validators: {
							notEmpty: {
								message: 'masukkan jumlah barang'
							}
						}
					}
				}
			});
		});
	</script>

</body>
</html>