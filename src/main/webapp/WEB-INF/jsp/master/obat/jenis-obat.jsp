<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Data Obat | Jenis Obat</title>
<link href='<c:url value="/resources/css/dataTables.bootstrap.min.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="<c:url value="/master/home"/>">Home</a></li>
		<li><a href="<c:url value="/master/obat/list"/>">Data Obat</a></li>
		<li class="active">Tambah Jenis Obat</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls col-sm-5">
		<div class="bs-example4">
			<div class="graphs">
				<h3 class="blank1">Form Jenis Obat</h3>
				<div class="tab-content">
					<div class="tab-pane active" id="horizontal-form">
						<form class="form-horizontal" action="jenis" method="post" id="formJenis">
							<div class="form-group">
								<label class="col-sm-4 control-label" id="labelId"></label>
								<div class="col-lg-8" id="inputId"></div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">Kode Jenis</label>
								<div class="col-lg-8">
									<input type="text" class="form-control1" name="kode" id="kode" placeholder="Kode Jenis Obat" value="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">Jenis Obat</label>
								<div class="col-lg-8">
									<input type="text" class="form-control1" name="jenis" id="jenis" placeholder="Jenis Obat" value="">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-5">
									<button type="submit" class="btn btn-custom btn-sm" name="login">Save</button>
								</div>
							</div>
						</form>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="xs tabls col-sm-7">
		<div class="bs-example4" data-example-id="contextual-table">
			<h5> </h5>
			<table class="table table-bordered" id="tbJenis">
				<thead>
					<tr>
						<th>Id</th>
						<th>Kode Jenis Obat</th>
						<th>Jenis Obat</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${jenisObat }" var="jenis">
						<tr>
							<td>${jenis.id }</td>
							<td>${jenis.kode }</td>
							<td>${jenis.jenis }</td>
							<td><button class="editt btn btn-link btn-xs" value="${jenis.id }" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o"></i></button>| 
								<a href="deleteJenis?id=${jenis.id }" class="btn btn-link btn-xs" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
	<script src="<c:url value='/resources/js/jquery.dataTables.min.js'/>"></script>
	<script src="<c:url value='/resources/js/dataTables.bootstrap.min.js'/>"></script>
	<script src="<c:url value='/resources/js/bootstrapValidator.js'/>"></script>
	<script type="text/javascript">
		$(document).ready(function() {
	    	$('#tbJenis').DataTable();
	    	$('[data-toggle="tooltip"]').tooltip();
	    	$('.editt').click(function() {
	    		var z = $(this).val();
	    		$('#labelId').text('Id');
	    		$('#inputId').html('<input type="text" class="form-control1" name="id" id="id" readonly>');
	    		$.ajax({
	    			type: 'post',
					url: 'editJenis',
					cache: false,
					data: ({name : z}),
					success: function(data) {
						$('#id').val(data.id);
						$('#kode').val(data.kode);
						$('#jenis').val(data.jenis);
					}
	    		});
	    	});
	    	$('#formJenis').bootstrapValidator({
	    		container: 'tooltip',
	    		feedbackIcons: {
	                valid: 'glyphicon glyphicon-ok',
	                invalid: 'glyphicon glyphicon-remove',
	                validating: 'glyphicon glyphicon-refresh'
	            },
	    		fields: {
	    			id: {
	    				validators: {
	    					notEmpty: {
	                            message: 'asd'
	                        },
	    				}
	    			},
	    			kode: {
	    				validators: {
	    					notEmpty: {
	                            message: 'masukkan kode jenis obat'
	                        },
	    				}
	    			},
	    			jenis: {
	    				validators: {
	    					notEmpty: {
	                            message: 'jenis obat tidak boleh kosong'
	                        },
	    				}
	    			}
	    		}
	    	});
		});
	</script>

</body>
</html>