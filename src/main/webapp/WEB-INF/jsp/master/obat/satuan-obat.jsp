<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Data Obat | Satuan Obat</title>
<link href='<c:url value="/resources/css/dataTables.bootstrap.min.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="<c:url value="/master/home"/>">Home</a></li>
		<li><a href="<c:url value="/master/obat/list"/>">Data Obat</a></li>
		<li class="active">Satuan Obat</li>
	</ol>
	
	<!-- panel -->
	<div class="bs-example5">
		<div class="row">
			
			<div class="col-lg-5">
				<h3 class="blank1">Form Satuan Obat</h3>
				
				<form class="form-horizontal" action="satuan" method="post" id="formSatuan">
					<div class="form-group">
						<label class="col-sm-4 control-label" id="labelId"></label>
						<div class="col-lg-8" id="inputId"></div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Kode Satuan</label>
						<div class="col-lg-8">
							<input type="text" class="form-control1" name="kode" id="kode" placeholder="Kode Satuan">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Satuan Obat</label>
						<div class="col-lg-8">
							<input type="text" class="form-control1" name="satuan" id="satuan" placeholder="Satuan Obat">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-5">
							<button type="submit" class="btn btn-custom btn-sm">Save</button>
							<button id="btnReset" type="reset" class="btn btn-custom btn-sm">Reset</button>
						</div>
					</div>
				</form>
			</div>
			
			<div class="col-lg-7">
				<h3 class="blank1">Tabel Satuan Obat</h3>
				
				<table class="table table-bordered" id="tbSatuan">
					<thead>
						<tr>
							<th>Id</th>
							<th>Kode Satuan</th>
							<th>Satuan Obat</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${satuanObat }" var="satuan">
							<tr>
								<td>${satuan.id }</td>
								<td>${satuan.kode }</td>
								<td>${satuan.satuan }</td>
								<td><button class="edit btn btn-link btn-xs" value="${satuan.id }" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o"></i></button>| 
									<a href="deleteSatuan?id=${satuan.id }" class="btn btn-link btn-xs" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></a>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		
		</div>
	</div>
	
	<script src="<c:url value='/resources/js/jquery.dataTables.min.js'/>"></script>
	<script src="<c:url value='/resources/js/dataTables.bootstrap.min.js'/>"></script>
	<script src="<c:url value='/resources/js/bootstrapValidator.js'/>"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();  
			$('#tbSatuan').DataTable();
			$('#btnReset').hide();
			
	    	$('.edit').click(function(){
	    		var idSatuan = $(this).val();
	    		$('#labelId').text('Id');
	    		$('#inputId').html('<input type="text" class="form-control1" name="id" id="id" placeholder="Kode Tipe Obat" readonly>');
	    		$('#btnReset').show();
	    		$.ajax({
	    			type: 'post',
					url: 'editSatuan',
					cache: false,
					data: ({id : idSatuan}),
					success: function(response) {
						$('#id').val(response.id);
						$('#kode').val(response.kode);
						$('#satuan').val(response.satuan);
					}
	    		});
	    	});
	    	
	    	$('#btnReset').click(function(){
	    		$('#labelId').text('');
	    		$('#inputId').html('');
	    		$(this).hide();
	    	});
	    	$('#formSatuan').bootstrapValidator({
	    		container: 'tooltip',
	    		feedbackIcons: {
	                valid: ' ',
	                invalid: 'glyphicon glyphicon-remove',
	                validating: 'glyphicon glyphicon-refresh'
	            },
	    		fields: {
	    			id: {
	    				validators: {
	    					notEmpty: {
	                            message: 'id kosong'
	                        },
	    				}
	    			},
	    			kode: {
	    				validators: {
	    					notEmpty: {
	                            message: 'masukkan kode satuan obat'
	                        },
	    				}
	    			},
	    			satuan: {
	    				validators: {
	    					notEmpty: {
	                            message: 'satuan obat tidak boleh kosong'
	                        },
	    				}
	    			}
	    		}
	    	});
		});
	</script>

</body>
</html>