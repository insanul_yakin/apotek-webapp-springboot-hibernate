<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Data Obat | Tipe Obat</title>
<link href='<c:url value="/resources/css/dataTables.bootstrap.min.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="<c:url value="/master/home"/>">Home</a></li>
		<li><a href="<c:url value="/master/obat/list"/>">Data Obat</a></li>
		<li class="active">Tambah Tipe Obat</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls col-sm-5">
		<div class="bs-example4">
			<div class="graphs">
				<h3 class="blank1">Tambah Tipe Obat</h3>
				<div class="tab-content">
					<div class="tab-pane active" id="horizontal-form">
						<form class="form-horizontal" action="tipe" method="post" id="formTipe">
							<div class="form-group" id="formId">
								<label class="col-sm-4 control-label" id="labelId"></label>
								<div class="col-lg-8" id="inputId">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">Kode Tipe</label>
								<div class="col-lg-8">
									<input type="text" class="form-control1" name="kode" id="kode" placeholder="Kode Tipe Obat">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">Tipe Obat</label>
								<div class="col-lg-8">
									<input type="text" class="form-control1" name="tipe" id="tipe" placeholder="Tipe Obat">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-5">
									<button type="submit" class="btn btn-custom btn-sm" name="login">Save</button>
									<button type="button" class="btn btn-custom btn-sm" name="login" id="btn">Button</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="xs tabls col-sm-7">
		<div class="bs-example4" data-example-id="contextual-table">
			<h5> </h5>
			<table class="table table-bordered" id="tbTipe">
				<thead>
					<tr>
						<th>Id</th>
						<th>Kode Jenis Obat</th>
						<th>Jenis Obat</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${tipeObat }" var="tipe">
						<tr>
							<td>${tipe.id }</td>
							<td>${tipe.kode }</td>
							<td>${tipe.tipe }</td>
							<td><button class="edit btn btn-link btn-xs" value="${tipe.id }" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o"></i></button>| 
								<a href="deleteTipe?id=${tipe.id }" class="btn btn-link btn-xs" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
	<script src="<c:url value='/resources/js/jquery.dataTables.min.js'/>"></script>
	<script src="<c:url value='/resources/js/dataTables.bootstrap.min.js'/>"></script>
	<script src="<c:url value='/resources/js/bootstrapValidator.js'/>"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#tbTipe').DataTable();
	    	$('.edit').click(function(){
	    		var x = $(this).val();
	    		$('#labelId').text("Id");
	    		$('#inputId').html('<input type="text" class="form-control1" name="id" id="id" readonly>');
	    		$.ajax({
	    			type: 'post',
					url: 'editTipe',
					cache: false,
					data: ({name : x}),
					success: function(data) {
						$('#id').val(data.id);
						$('#kode').val(data.kode);
						$('#tipe').val(data.tipe);
					}
	    		});
	    	});
	    	$('#formTipe').bootstrapValidator({
	    		container: 'tooltip',
	    		feedbackIcons: {
	                valid: ' ',
	                invalid: 'glyphicon glyphicon-remove',
	                validating: 'glyphicon glyphicon-refresh'
	            },
	    		fields: {
	    			id: {
	    				validators: {
	    					notEmpty: {
	                            message: 'id kosong'
	                        },
	    				}
	    			},
	    			kode: {
	    				validators: {
	    					notEmpty: {
	                            message: 'masukkan kode tipe obat'
	                        },
	    				}
	    			},
	    			tipe: {
	    				validators: {
	    					notEmpty: {
	                            message: 'tipe obat tidak boleh kosong'
	                        },
	    				}
	    			}
	    		}
	    	});
		});
	</script>

</body>
</html>