<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href='<c:url value="/resources/css/bootstrap-select.css"/>' rel='stylesheet'>
<link href='<c:url value="/resources/css/dataTables.bootstrap.min.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="#">Home</a></li>
		<li><a href="#">Library</a></li>
		<li class="active">Data</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls col-lg-6">
		<div class="bs-example4">
			<div class="graphs">
				<h3 class="blank1">Basic Forms</h3>
				<div class="tab-content">
					<div class="tab-pane active" id="horizontal-form">
						<form class="form-horizontal" action="add" method="post">
							<div class="form-group">
								<label class="control-label col-sm-2"></label>
								<div class="col-sm-6">
									<div id="resep" class="btn-group" data-toggle="buttons">
										<label class="btn btn-custom" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
											<input type="radio" name="resep" value="1"> &nbsp; Resep &nbsp;
										</label>
										<label class="btn btn-custom" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
											<input type="radio" name="resep" value="0" checked>Non Resep
										</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Tanggal</label>
								<div class="col-lg-8">
									<input type="text" class="form-control1" name="tgl" id="tgl">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Nama</label>
								<div class="col-lg-8">
									<input type="text" class="form-control1" name="nama" id="nama" placeholder="Masukkan Nama">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Total</label>
								<div class="col-lg-8">
									<input type="text" class="form-control1" id="total" name="totalHarga" value='<%= request.getSession().getAttribute("total") %>' placeholder="Total Harga" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Nominal Uang</label>
								<div class="col-lg-8">
									<input type="text" class="form-control1" id="nominalUang" name="nominalUang" placeholder="Nominal Uang">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Jumlah Kembalian</label>
								<div class="col-lg-8">
									<input type="text" class="form-control1" id="kembalian" name="kembalian" placeholder="Jumlah Kembalian" readonly>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit" class="btn btn-custom btn-sm">Save</button>
									<button type="reset" class="btn btn-custom btn-sm">Reset</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- panel -->
	<div class="xs tabls col-lg-6">
		<div class="bs-example4">
			<div class="graphs">
				<div class="tab-content">
					<div class="tab-pane active" id="horizontal-form">
						<h3 class="blank1">Detil Obat</h3>
						<form class="form-horizontal" action="addCart" method="post" id="formCart">
							<div class="form-group">
								<label class="col-sm-3 control-label">Nama Obat</label>
								<div class="col-sm-8">
									<select class="selectpicker" name="obat" id="obat" data-hide-disabled="true" data-live-search="true">
										<option value="">-- Pilih Obat ---</option>
										<c:forEach items="${obat }" var="obat">
											<option value="${obat.id }">${obat.namaObat }</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Kode Obat</label>
								<div class="col-lg-8">
									<input type="text" class="form-control1" id="kodeObat" placeholder="Kode Obat" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Harga</label>
								<div class="col-lg-8">
									<input type="text" class="form-control1" id="harga" placeholder="Harga" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Jumlah</label>
								<div class="col-lg-4">
									<input type="text" class="form-control1" name="jmlBeli" id="jml" placeholder="Yang dibeli">
								</div>
								<div class="col-lg-4">
									<input type="text" class="form-control1" id="tersedia" placeholder="Stok Tersedia" readonly>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit" id="f" class="btn btn-custom btn-sm">Add Cart</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="xs tabls col-lg-12">
		<h3> </h3>
	</div>
	<!-- panel -->
	<div class="xs tabls col-lg-12">
		<div class="bs-example4" data-example-id="contextual-table">
			<h3 class="blank1">List Detil Obat</h3>
			<table id="tbCart" class="table table-bordered">
				<thead>
					<tr>
						<th>Kode Obat</th>
						<th>Nama Obat</th>
						<th>Harga</th>
						<th>Jumlah dibeli</th>
						<th>Total Harga</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items='<%= request.getSession().getAttribute("cart") %>' var="cart">
						<tr id="cart">
							<td id="ko">${cart.obat.kode }</td>
							<td>${cart.obat.namaObat }</td>
							<td>${cart.obat.hargaJual }</td>
							<td id="jml1">${cart.jmlBeli }</td>
							<td>${cart.totalHarga }</td>
							<td><a href="remove?kode=${cart.obat.kode }" onclick="return confirm('are you sure?')">Remove</a></td>
						</tr>
					</c:forEach>
				</tbody>
				<tfoot>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td><%= request.getSession().getAttribute("total") %></td>
						<td></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	
	<script src="<c:url value='/resources/js/bootstrap-select.js'/>"></script>
	<script src="<c:url value='/resources/js/jquery.dataTables.min.js'/>"></script>
	<script src="<c:url value='/resources/js/dataTables.bootstrap.min.js'/>"></script>
    <script type="text/javascript">
    $(document).ready(function() {
    	var date = new Date();
		var mont = ["January", "February", "March", "April", "May", "June",
		            "July", "August", "September", "October", "November", "December"
		            ];
		var tgl = date.getDate() + "-" + mont[date.getMonth()] + "-" + date.getFullYear();
		$('#tgl').val(tgl);
    	$('#tbCart').DataTable();
		$('#obat').change(function(){
			var id = $(this).val();
			$.ajax({
				type: 'post',
				url: 'get',
				cahce: false,
				data: ({id : id}),
				success: function(response) {
					$('#kodeObat').val(response.kode);
					$('#harga').val(response.hargaJual);
					var a = true;
					var b;
					$('tr#cart').each(function() {
						var kode = $(this).find('td#ko').text();
						var jml = $(this).find('td#jml1').text();
						if (kode == response.kode) {
							var total = response.stok - jml;
							b = total;
							a = false;
							return false;
						}
					});
					
					if(a == true) {
						$('#tersedia').val("/ "+response.stok);
					} else {
						$('#tersedia').val("/ "+b);
					}
				}
			});
		});
		$('#nominalUang').change(function () {
            var nominal = $('#nominalUang').val();
            var total = $('#total').val();
            var hasil = nominal - total;
            $("#kembalian").val(hasil);
        })
	});
    </script>
</body>
</html>
