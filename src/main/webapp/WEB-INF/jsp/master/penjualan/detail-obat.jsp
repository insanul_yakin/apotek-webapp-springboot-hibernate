<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href='<c:url value="/resources/css/dataTables.bootstrap.min.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="/master/home">Home</a></li>
		<li class="active">Penerimaan Obat</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4" data-example-id="contextual-table">
			<br><br>
			<table id="example" class="table table-bordered">
				<thead>
					<tr>
						<th>Id</th>
						<th>Kode Obat</th>
						<th>Nama Obat</th>
						<th>Harga</th>
						<th>Jumlah dibeli</th>
						<th>Total Harga</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${detilPenjualan }" var="dp">
						<tr>
							<td>${dp.obat.id }</td>
							<td>${dp.obat.kode }</td>
							<td>${dp.obat.namaObat }</td>
							<td>${dp.obat.hargaJual }</td>
							<td>${dp.jmlBeli }</td>
							<td>${dp.totalHarga }</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
	<script src="<c:url value='/resources/js/jquery.dataTables.min.js'/>"></script>
	<script src="<c:url value='/resources/js/dataTables.bootstrap.min.js'/>"></script>
	<script type="text/javascript">
		$(document).ready(function() {
	    	$('#example').DataTable();
		});
	</script>
</body>
</html>
