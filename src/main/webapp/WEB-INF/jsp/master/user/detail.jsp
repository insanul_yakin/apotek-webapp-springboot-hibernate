<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Detil User</title>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="<c:url value='/master/home'/>">Home</a></li>
		<li><a href="<c:url value='/master/user/list'/>">Data User</a></li>
		<li class="active">Edit User</li>
	</ol>
	
	<!-- panel detil user -->
	<div class="xs tabls" id="detilUser">
		<div class="bs-example4">
			<div class="graphs">
				<h3 class="blank1">User Detail</h3>
				<div class="tab-content">
					<div class="tab-pane active" id="horizontal-form">
						<form class="form-horizontal" action="add" method="post">
							<div class="form-group hide">
								<label class="col-sm-2 control-label">User ID</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="id" value="${user.id }" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Username</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="username" value="${user.username }" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Status</label>
								<div class="col-lg-5">
									<div id="status" class="btn-group" data-toggle="buttons">
										<label class="btn btn-custom" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
											<input type="radio" name="status" value="1" ${user.status == 1 ? 'checked' : '' }> &nbsp; Aktif &nbsp;
										</label>
										<label class="btn btn-custom" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
											<input type="radio" name="status" value="0" ${user.status == 0 ? 'checked' : '' }> Tidak Aktif
										</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="selector1" class="col-sm-2 control-label">Role</label>
								<div class="col-sm-5">
									<c:forEach items="${user.role }" var="users">
										<select name="role" id="role" class="form-control1">
											<option>-- Role --</option>
											<c:forEach items="${role }" var="role">
												<option value="${role.id }" ${role.id == users.id ? 'selected' : ''}>${role.authority }</option>
											</c:forEach>
										</select>
									</c:forEach>
									<button type="button" id="btnUbahPasswd" class="btn btn-link btn-sm">Ubah Password User</button>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-5">
									<button type="submit" class="btn btn-custom btn-sm" name="login">Save</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- form ubah passwd -->
	<div class="xs tabls" id="ubahPasswd">
		<div class="bs-example4">
			<div class="graphs">
				<h3 class="blank1">Ubah Password User</h3>
				<div class="tab-content">
					<div class="tab-pane active" id="horizontal-form">
						
						<form class="form-horizontal" id="formUbahPasswd">
							<div class="form-group hide">
								<label class="col-sm-2 control-label">User ID</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="id" value="${user.id }" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Password</label>
								<div class="col-lg-5">
									<input type="password" class="form-control1" name="password">
								</div>
							</div>
<!-- 							<div class="form-group"> -->
								<div class="col-sm-offset-2 col-sm-5" id="result"></div>
<!-- 							</div> -->
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-5">
									<button type="button" id="kembali" class="btn btn-custom btn-sm"><<<</button>
									<button type="button" id="lanjut" class="btn btn-custom btn-sm">>>></button>
								</div>
							</div>
						</form>
						
						<form action="changePasswd" method="post" class="form-horizontal" id="formPasswdBaru">
							<div class="form-group hide">
								<label class="col-sm-2 control-label">User ID</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="id" value="${user.id }" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">New Password</label>
								<div class="col-lg-5">
									<input type="password" class="form-control1" id="passwd" name="password" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Retype New Password</label>
								<div class="col-lg-5">
									<input type="password" class="form-control1" id="passwd" required>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-lg-5">
									<button type="submit" class="btn btn-custom btn-sm">Save</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$('#ubahPasswd').hide();
			$('#btnUbahPasswd').click(function(){
				$('#detilUser').hide('slow');
				$('#ubahPasswd').show('slow');
				$('#formPasswdBaru').hide();
// 				$("#passwd").prop('disabled', true);
			});
			$('#lanjut').click(function(){
				$.ajax({
					type: 'post',
					url: 'getPasswd',
					cache: false,
					data: $('#formUbahPasswd').serializeArray(),
					success: function(response) {
						if(response == "1"){
							$('#result').text('');
							$('#formPasswdBaru').show('slow');
						} else {
							$('#result').text('password salah mohon masukkan kembali');
							$('#formPasswdBaru').hide('slow');
						}
					}
				});
			});
			$('#kembali').click(function(){
				$('#detilUser').show('slow');
				$('#ubahPasswd').hide('slow');
			});
		});
	</script>

</body>
</html>