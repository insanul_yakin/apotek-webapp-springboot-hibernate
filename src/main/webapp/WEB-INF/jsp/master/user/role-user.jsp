<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href='<c:url value="/resources/css/dataTables.bootstrap.min.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="<c:url value='/master/home'/>">Home</a></li>
		<li><a href="<c:url value='/master/user/list'/>">Data User</a></li>
		<li class="active">Role</li>
	</ol>
	
	<!-- panel -->
	<div class="bs-example1">
		<div class="row">
			
			<div class="col-lg-5">
				<h3 class="blank1">Role</h3>
				
				<form action="addRole" method="post"class="form-horizontal" id="formRole">
					<div class="form-group">
						<label class="col-sm-3 control-label" id="labelId"></label>
						<div class="col-lg-8" id="inputId"></div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Authority</label>
						<div class="col-lg-8">
							<input type="text" class="form-control1" name="authority" id="role" placeholder="role" required>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-custom btn-sm" name="login">Save</button>
							<button id="btnReset" type="reset" class="btn btn-custom btn-sm">Reset</button>
						</div>
					</div>
				</form>
			</div>
			
			<div class="col-lg-7">
				<h3 class="blank1">Tabel Role</h3>
				
				<table class="table table-bordered" id="tbRole">
					<thead>
						<tr>
							<th>Id</th>
							<th>Role</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${roles }" var="role">
							<tr>
								<td>${role.id }</td>
								<td>${role.authority }</td>
								<td><button class="edit btn btn-link btn-sm" value="${role.id }"><i class="fa fa-pencil-square-o"></i> Edit</button>| 
									<a href="delRole?id=${role.id }" class="btn btn-link btn-sm"><i class="fa fa-times"> Delete</i></a>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		
		</div>
	</div>
	
	<script src="<c:url value='/resources/js/jquery.dataTables.min.js'/>"></script>
	<script src="<c:url value='/resources/js/dataTables.bootstrap.min.js'/>"></script>
	<script src="<c:url value='/resources/js/bootstrapValidator.js'/>"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#tbRole').DataTable();
			$('#btnReset').hide();
	    	$('.edit').click(function(){
	    		var x = $(this).val();
	    		$('#labelId').text("Id");
	    		$('#inputId').html('<input type="text" class="form-control1" name="id" id="id" readonly>').fadeIn('slow');
	    		$('#btnReset').show();
	    		$.ajax({
	    			type: 'post',
					url: 'getRole',
					cache: false,
					data: ({id : x}),
					success: function(response) {
						$('#id').val(response.id);
						$('#role').val(response.authority);
					}
	    		});
	    	});
	    	$('#btnReset').click(function(){
	    		$('#labelId').text('');
	    		$('#inputId').html('');
	    		$(this).hide();
	    	});
	    	$('#formRole').bootstrapValidator({
	    		fields: {
	    			authority: {
	    				validators: {
		                    notEmpty: {
		                        message: 'masukkan role'
		                    },
		                    regexp: {
		                        regexp: /^[a-zA-Z0-9\._]+$/,
		                        message: 'Role hanya bisa terdiri dari abjad, angka, titik dan garis bawah'
		                    }
		                }
	    			}
	    		}
	    	});
		});
	</script>

</body>
</html>