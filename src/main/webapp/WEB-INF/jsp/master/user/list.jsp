<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Data User</title>
<link href='<c:url value="/resources/css/dataTables.bootstrap.min.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="<c:url value='/master/home'/>">Home</a></li>
		<li class="active">Data User</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4" data-example-id="contextual-table">
			<a class="btn btn-custom" href="add"><i class="fa fa-user-plus"></i> Add User</a>
			<a class="btn btn-custom" href="role"><i class="fa fa-user-plus"></i> Role User</a>
			<br><br>
			<table id="example" class="table table-bordered">
				<thead>
					<tr>
						<th>Id</th>
						<th>Username</th>
						<th>Status</th>
						<th>Role</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${users }" var="user">
						<tr>
							<td>${user.id }</td>
							<td>${user.username }</td>
							<td> <c:out value="${user.status == '1' ? 'Aktif' : 'Tidak Aktif'}" /></td>
							<td><c:forEach items="${user.role }" var="role">${role.authority }</c:forEach></td>
							<td><a href="edit?id=${user.id }" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil-square-o"></i></a> | 
								<a href="delete?id=${user.id }" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times"></i></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
	<script src="<c:url value='/resources/js/jquery.dataTables.min.js'/>"></script>
	<script src="<c:url value='/resources/js/dataTables.bootstrap.min.js'/>"></script>
	<script type="text/javascript">
		$(document).ready(function() {
	    	$('#example').DataTable();
	    	$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
</body>
</html>