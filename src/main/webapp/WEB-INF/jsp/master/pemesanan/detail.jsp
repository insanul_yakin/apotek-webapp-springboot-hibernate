<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href='<c:url value="/resources/css/datepicker.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="<c:url value="/master/home"/>">Home</a></li>
		<li><a href="<c:url value="/master/pemesanan/add"/>">Pemesanan</a></li>
		<li><a href="<c:url value="/master/pemesanan/list"/>">List</a></li>
		<li class="active">Detil</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4">
			<div class="graphs">
				<h3 class="blank1">Basic Forms</h3>
				<div class="tab-content">
					<div class="tab-pane active" id="horizontal-form">
						<form action="add" method="post" class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-2 control-label">ID</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="id" value="${pesan.id }" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">No Pemesanan</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="noPemesanan" value="${pesan.noPemesanan }">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Tanggal</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="tglPemesanan" id="date" value="<fmt:formatDate value='${pesan.tglPemesanan }' pattern='dd-MMMM-yyyy'/>" readonly>
								</div>
							</div>
							<div class="form-group">
								<label for="selector1" class="col-sm-2 control-label">Nama Supplier</label>
								<div class="col-sm-5">
									<select name="supplier" class="form-control1">
										<option value="0">-- Pilih Supplier --</option>
										<c:forEach items="${suppliers }" var="supplier">
											<option value="${supplier.id }" ${supplier.id == pesan.supplier.id ? 'selected' : ''}>${supplier.namaSupplier }</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="selector1" class="col-sm-2 control-label">Kode Obat</label>
								<div class="col-sm-5">
									<select name="obat" id="obat" class="form-control1">
										<option value="0">-- Pilih Kode Obat --</option>
										<c:forEach items="${obat }" var="obat">
											<option value="${obat.id }" ${obat.id == pesan.obat.id ? 'selected' : ''}>${obat.kode }</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Obat</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="namaObat" id="namaObat" value="${pesan.obat.namaObat }" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jenis Obat</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="jenisObat" id ="jenisObat" value="${pesan.obat.jenisObat.jenis }" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Satuan</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="satuan" id="satuan" value="${pesan.obat.satuanObat.satuan }" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jumlah Pesan</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="jmlPemesanan" value="${pesan.jmlPemesanan }">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-5">
									<button type="submit" class="btn btn-custom btn-sm">Save</button>
									<button type="reset" class="btn btn-custom btn-sm">Reset</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="<c:url value='/resources/js/bootstrap-datepicker.js'/>"></script>
    <script type="text/javascript">
    $(document).ready(function() {
    	$('#hide').hide();
		$('#date').datepicker({
			format: 'dd-MM-yyyy',
			autoclose: true,
			todayHighlight: true
		});
		$('#obat').change(function(){
			var id = $(this).val();
			$.ajax({
				type: 'post',
				url: 'get',
				cahce: false,
				data: ({id : id}),
				success: function(response) {
					$('#namaObat').val(response.namaObat);
					$('#jenisObat').val(response.jenisObat.jenis);
					$('#satuan').val(response.satuanObat.satuan);
				}
			});
		});
	});
    </script>
</body>
</html>
