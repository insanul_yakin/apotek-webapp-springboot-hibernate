<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Data Supplier | add</title>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="<c:url value='/master/home'/>">Home</a></li>
		<li><a href="<c:url value='/master/supplier/list'/>">Data Supplier</a></li>
		<li class="active">Add</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4">
			<div class="graphs">
				<h3 class="blank1">Form Tambah Supplier</h3>
				<div class="tab-content">
					<div class="tab-pane active" id="horizontal-form">
						<form class="form-horizontal" action="add" method="post" id="formSupplier">
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Supplier</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="namaSupplier" placeholder="Nama Supplier">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Alamat Supplier</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="alamatSupplier" placeholder="Alamat Supplier">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Kota Supplier</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="kotaSupplier" placeholder="Kota Supplier">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Telepon Supplier</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="tlpSupplier" placeholder="Telepon Supplier">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">E-Mail</label>
								<div class="col-lg-5">
									<input type="email" class="form-control1" name="email" placeholder="example@domain.com">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-5">
									<button type="submit" class="btn btn-custom btn-sm">Save</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<script src="<c:url value='/resources/js/bootstrapValidator.js'/>"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#formSupplier').bootstrapValidator({
				fields: {
					namaSupplier: {
						validators: {
							notEmpty: {
								message: 'Nama supplier tidak boleh kosong'
							}
						}
					},
					alamatSupplier: {
						validators: {
							notEmpty: {
								message: 'Alamat Supplier tidak boleh kosong'
							}
						}
					},
					tlpSupplier: {
						validators: {
		                    regexp: {
		                        regexp: /^[0-9]+$/,
		                        message: 'hanya bisa terdiri dari angka'
		                    }
		                }
					},
					email: {
						validators: {
							emailAddress: {
		                        message: 'The input is not a valid email address'
		                    }
						}
					}
				}
			});
		});
	</script>
</body>
</html>