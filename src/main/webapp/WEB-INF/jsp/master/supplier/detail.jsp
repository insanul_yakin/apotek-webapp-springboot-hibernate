<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="#">Home</a></li>
		<li><a href="#">Data Supplier</a></li>
		<li class="active">Tambah Supplier</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4">
			<div class="graphs">
				<h3 class="blank1">Form Tambah Supplier</h3>
				<div class="tab-content">
					<div class="tab-pane active" id="horizontal-form">
						<form class="form-horizontal" action="add" method="post">
							<div class="form-group">
								<label class="col-sm-2 control-label">ID Supplier</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="id" value="${supplier.id }" readonly="readonly">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Supplier</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="namaSupplier" value="${supplier.namaSupplier }">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Alamat Supplier</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="alamatSupplier" value="${supplier.alamatSupplier }">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Kota Supplier</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="kotaSupplier" value="${supplier.kotaSupplier }">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Telepon Supplier</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="tlpSupplier" value="${supplier.tlpSupplier }">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">E-Mail</label>
								<div class="col-lg-5">
									<input type="email" class="form-control1" name="email" value="${supplier.email }">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-5">
									<button type="submit" class="btn btn-custom btn-sm">Save</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>