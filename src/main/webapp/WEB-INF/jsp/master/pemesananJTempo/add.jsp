<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href='<c:url value="/resources/css/datepicker.css"/>' rel='stylesheet'>
</head>
<body>
	<!-- Breadcrumbs -->
	<ol class="breadcrumb">
		<li><a href="#">Home</a></li>
		<li><a href="#">Library</a></li>
		<li class="active">Data</li>
	</ol>
	
	<!-- panel -->
	<div class="xs tabls">
		<div class="bs-example4">
			<div class="graphs">
				<h3 class="blank1">Form Penerimaan Obat</h3>
				<div class="tab-content">
					<div class="tab-pane active" id="horizontal-form">
						<form action="add" method="post" class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-2 control-label">No Faktur</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" name="noFaktur" placeholder="Nomor Faktur">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">No Pemesanan</label>
								<div class="col-sm-5">
									<select name="pemesanan" id="noPesan" class="form-control1">
										<option>-- No Pemesanan --</option>
										<c:forEach items="${pemesanan }" var="pemesanan">
											<c:if test="${pemesanan.status != 1 }">
												<option value="${pemesanan.id }">${pemesanan.noPemesanan }</option>
											</c:if>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Tanggal Pemesanan</label>
								<div class="col-lg-3">
									<input type="text" class="form-control1" id="tglPesan" placeholder="Tanggal Pemesanan" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Supplier</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" id="namaSupplier" placeholder="Nama Supplier" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Obat</label>
								<div class="col-lg-5">
									<input type="text" class="form-control1" id="namaObat" placeholder="Nama Obat" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Tanggal Jatuh Tempo</label>
								<div class="col-lg-3">
									<input type="text" class="form-control1" name="tglJTempo" id="date" placeholder="Tanggal Jatuh Tempo" readonly>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-5">
									<button type="submit" class="btn btn-custom btn-sm">Save</button>
									<button type="reset" class="btn btn-custom btn-sm">Reset</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="<c:url value='/resources/js/bootstrap-datepicker.js'/>"></script>
    <script type="text/javascript">
    $(document).ready(function() {
		$('#date').datepicker({
			format: 'dd-MM-yyyy',
			autoclose: true,
			todayHighlight: true
		});
		$('#noPesan').change(function(){
			var id = $(this).val();
			$.ajax({
				type: 'post',
				url: 'get',
				cahce: false,
				data: ({id : id}),
				success: function(response) {
					var date = new Date(response.tglPemesanan);
					var mont = ["January", "February", "March", "April", "May", "June",
					            "July", "August", "September", "October", "November", "December"
					            ];
					var tgl = date.getDate() + "-" + mont[date.getMonth()] + "-" + date.getFullYear();
					$('#tglPesan').val(tgl);
					$('#namaSupplier').val(response.supplier.namaSupplier);
					$('#namaObat').val(response.obat.namaObat);
				}
			});
		});
	});
    </script>
</body>
</html>